{
    "id": "191f5cdb-4f1d-449a-bcfd-14113c0f32db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 2,
    "bbox_right": 11,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46fe2a73-d060-4ad4-9c3c-42596d08c489",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "191f5cdb-4f1d-449a-bcfd-14113c0f32db",
            "compositeImage": {
                "id": "cd1c3683-1e65-448a-8fd3-b338be2f39e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46fe2a73-d060-4ad4-9c3c-42596d08c489",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8244ac07-d83b-42aa-9d16-f9c3a1ba8158",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46fe2a73-d060-4ad4-9c3c-42596d08c489",
                    "LayerId": "5a7a085a-c8d6-499a-be18-f11bb662bd0b"
                }
            ]
        },
        {
            "id": "ba072414-d9b7-455c-ab6a-fd78d80ac8c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "191f5cdb-4f1d-449a-bcfd-14113c0f32db",
            "compositeImage": {
                "id": "482cf156-bbb4-4f84-b0d6-87d2470f0d44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba072414-d9b7-455c-ab6a-fd78d80ac8c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61185c33-ad6e-435d-b3fd-1b50ad290166",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba072414-d9b7-455c-ab6a-fd78d80ac8c2",
                    "LayerId": "5a7a085a-c8d6-499a-be18-f11bb662bd0b"
                }
            ]
        },
        {
            "id": "e9d45ac8-e527-4b54-87c3-9ea19bcb2b62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "191f5cdb-4f1d-449a-bcfd-14113c0f32db",
            "compositeImage": {
                "id": "337384b4-13aa-4117-af78-860584610fcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9d45ac8-e527-4b54-87c3-9ea19bcb2b62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "696471d1-b333-49f9-bd8f-a79b10efe5fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9d45ac8-e527-4b54-87c3-9ea19bcb2b62",
                    "LayerId": "5a7a085a-c8d6-499a-be18-f11bb662bd0b"
                }
            ]
        },
        {
            "id": "33d30905-9a5b-450c-97b4-6de32b18c7d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "191f5cdb-4f1d-449a-bcfd-14113c0f32db",
            "compositeImage": {
                "id": "e2a2fb24-78a7-4293-a5ce-5fa37e943614",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33d30905-9a5b-450c-97b4-6de32b18c7d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d82b5b0b-9ca1-408b-a47c-0273250564a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33d30905-9a5b-450c-97b4-6de32b18c7d9",
                    "LayerId": "5a7a085a-c8d6-499a-be18-f11bb662bd0b"
                }
            ]
        },
        {
            "id": "582fe3b6-4de7-4eec-88f6-d412a37af952",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "191f5cdb-4f1d-449a-bcfd-14113c0f32db",
            "compositeImage": {
                "id": "2ed06680-5c40-4d7c-a933-001926526a55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "582fe3b6-4de7-4eec-88f6-d412a37af952",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f12adfea-3fcb-4c70-b0e6-eae32ffaabb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "582fe3b6-4de7-4eec-88f6-d412a37af952",
                    "LayerId": "5a7a085a-c8d6-499a-be18-f11bb662bd0b"
                }
            ]
        },
        {
            "id": "493d8c3a-ccbf-49ad-8476-735dbcda1517",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "191f5cdb-4f1d-449a-bcfd-14113c0f32db",
            "compositeImage": {
                "id": "2255ab2c-f8ea-47c9-8afa-d8b863432341",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "493d8c3a-ccbf-49ad-8476-735dbcda1517",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb7fc5c0-8258-455c-ae36-512517f29419",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "493d8c3a-ccbf-49ad-8476-735dbcda1517",
                    "LayerId": "5a7a085a-c8d6-499a-be18-f11bb662bd0b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "5a7a085a-c8d6-499a-be18-f11bb662bd0b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "191f5cdb-4f1d-449a-bcfd-14113c0f32db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 17
}