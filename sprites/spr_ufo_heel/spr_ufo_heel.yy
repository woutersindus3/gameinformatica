{
    "id": "440f00aa-4234-41c3-9878-7e1fbc71dbda",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ufo_heel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 40,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ab3e9a4-9bdb-4127-b849-084c5d7cbd38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "440f00aa-4234-41c3-9878-7e1fbc71dbda",
            "compositeImage": {
                "id": "879cb0f4-e7b7-4805-aabc-427dcf5048c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ab3e9a4-9bdb-4127-b849-084c5d7cbd38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92b3f2bb-835c-4c2f-aae4-072b29bca703",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ab3e9a4-9bdb-4127-b849-084c5d7cbd38",
                    "LayerId": "b38cad5a-80f8-4ee6-bdf9-2ef2462e8033"
                }
            ]
        },
        {
            "id": "79f67a99-14ad-4f4e-a097-486562b09969",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "440f00aa-4234-41c3-9878-7e1fbc71dbda",
            "compositeImage": {
                "id": "c58ee07e-254a-4dd3-b3cc-13db90819cea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79f67a99-14ad-4f4e-a097-486562b09969",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09041dfb-2f06-4e9f-8a44-855ee7482c31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79f67a99-14ad-4f4e-a097-486562b09969",
                    "LayerId": "b38cad5a-80f8-4ee6-bdf9-2ef2462e8033"
                }
            ]
        },
        {
            "id": "be83e9ff-211d-457e-a661-533f92d9ea4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "440f00aa-4234-41c3-9878-7e1fbc71dbda",
            "compositeImage": {
                "id": "2a662733-b6a4-42a8-819a-8e8856b6f752",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be83e9ff-211d-457e-a661-533f92d9ea4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4adfcfa-7b92-490e-b198-0a01501725d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be83e9ff-211d-457e-a661-533f92d9ea4d",
                    "LayerId": "b38cad5a-80f8-4ee6-bdf9-2ef2462e8033"
                }
            ]
        },
        {
            "id": "217fbf5a-4345-4c63-96f9-d16d3aa5edd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "440f00aa-4234-41c3-9878-7e1fbc71dbda",
            "compositeImage": {
                "id": "7b5ee9c7-5339-4628-945c-5d19c2ff0976",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "217fbf5a-4345-4c63-96f9-d16d3aa5edd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98f4abc1-fde7-444e-9e92-c378e5b18561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "217fbf5a-4345-4c63-96f9-d16d3aa5edd7",
                    "LayerId": "b38cad5a-80f8-4ee6-bdf9-2ef2462e8033"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "b38cad5a-80f8-4ee6-bdf9-2ef2462e8033",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "440f00aa-4234-41c3-9878-7e1fbc71dbda",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 41,
    "xorig": 20,
    "yorig": 11
}