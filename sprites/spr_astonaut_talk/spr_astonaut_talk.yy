{
    "id": "318894a5-22fc-40ff-af31-a89cd5912e2a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_astonaut_talk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 11,
    "bbox_right": 116,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "135ac3e5-e2a8-4a0b-a230-b02fa3826386",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "318894a5-22fc-40ff-af31-a89cd5912e2a",
            "compositeImage": {
                "id": "1f1e50f9-af75-4cc7-a519-dc69c189dc75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "135ac3e5-e2a8-4a0b-a230-b02fa3826386",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10824378-43ab-42be-824e-306bec238a86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "135ac3e5-e2a8-4a0b-a230-b02fa3826386",
                    "LayerId": "6ca78b55-edb3-435d-9a1a-cfcef87c7a1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "6ca78b55-edb3-435d-9a1a-cfcef87c7a1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "318894a5-22fc-40ff-af31-a89cd5912e2a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}