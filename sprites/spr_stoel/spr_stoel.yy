{
    "id": "c4f92ab1-cfed-4003-a102-23e880662b36",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stoel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46a42c57-7dbe-44c7-9845-019fc3a38d58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4f92ab1-cfed-4003-a102-23e880662b36",
            "compositeImage": {
                "id": "8cbccbed-248d-43b0-b0ce-d4ef314ec821",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46a42c57-7dbe-44c7-9845-019fc3a38d58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9d1cf7e-c572-405a-a677-3b5fbd4bdfdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46a42c57-7dbe-44c7-9845-019fc3a38d58",
                    "LayerId": "faf693dd-a2da-4f24-b786-5199baf06b08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "faf693dd-a2da-4f24-b786-5199baf06b08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4f92ab1-cfed-4003-a102-23e880662b36",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}