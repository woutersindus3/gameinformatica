{
    "id": "5a16b9a9-07fa-4743-a20b-60bb8e78063f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_collision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9357c3c0-7e58-43c2-9c58-d8f0754ea355",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a16b9a9-07fa-4743-a20b-60bb8e78063f",
            "compositeImage": {
                "id": "c7353e8d-2c2a-4818-8543-ad530e6ba5c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9357c3c0-7e58-43c2-9c58-d8f0754ea355",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ad44503-904f-4cae-bbe7-a3bf1003c7f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9357c3c0-7e58-43c2-9c58-d8f0754ea355",
                    "LayerId": "1095123b-c4f2-4591-bb80-9571c6c14194"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1095123b-c4f2-4591-bb80-9571c6c14194",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a16b9a9-07fa-4743-a20b-60bb8e78063f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 16,
    "yorig": 19
}