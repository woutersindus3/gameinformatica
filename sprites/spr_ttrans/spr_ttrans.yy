{
    "id": "656ad89e-513a-44cd-a008-becfcbd311e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ttrans",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf0baba3-3fe6-4aec-8168-ed60bc18249c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "656ad89e-513a-44cd-a008-becfcbd311e0",
            "compositeImage": {
                "id": "d3d974aa-c41a-4991-bff9-c55edef6627a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf0baba3-3fe6-4aec-8168-ed60bc18249c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbdee670-d118-4dc1-92d4-6f5101f9ea98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf0baba3-3fe6-4aec-8168-ed60bc18249c",
                    "LayerId": "1c2dced1-3a9d-48a3-b6f0-e8baea27d75b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1c2dced1-3a9d-48a3-b6f0-e8baea27d75b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "656ad89e-513a-44cd-a008-becfcbd311e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 16,
    "yorig": 19
}