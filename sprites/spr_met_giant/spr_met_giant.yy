{
    "id": "d7bae69b-f002-4baa-8d95-48927d387278",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_met_giant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 382,
    "bbox_left": 16,
    "bbox_right": 237,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b63917f-8877-4ed9-ac6c-3fefe585a3a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7bae69b-f002-4baa-8d95-48927d387278",
            "compositeImage": {
                "id": "9bf956f1-4fc0-49ad-846f-1699d89c2e16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b63917f-8877-4ed9-ac6c-3fefe585a3a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97699d0f-7c43-4446-b611-6dcf3d9c89e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b63917f-8877-4ed9-ac6c-3fefe585a3a2",
                    "LayerId": "1c0f68e4-bf20-495f-8c0b-f2ae71f0e124"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "1c0f68e4-bf20-495f-8c0b-f2ae71f0e124",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7bae69b-f002-4baa-8d95-48927d387278",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 125,
    "yorig": 200
}