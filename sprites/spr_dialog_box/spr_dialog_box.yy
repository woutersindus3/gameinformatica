{
    "id": "9cc42b75-d598-4030-a5f7-b0a6fa0566ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dialog_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8a782b2-68c3-470d-919d-41e547e63049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cc42b75-d598-4030-a5f7-b0a6fa0566ea",
            "compositeImage": {
                "id": "3ca5ce47-a280-476b-8527-71d632bb717d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8a782b2-68c3-470d-919d-41e547e63049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cca9ccf-11f9-4490-98a1-5e7b2e05a06e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8a782b2-68c3-470d-919d-41e547e63049",
                    "LayerId": "6524ba3d-c95c-45f1-b291-124a2e9acba7"
                },
                {
                    "id": "7b979b5b-c397-4c9b-b95c-2ae4d857a31d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8a782b2-68c3-470d-919d-41e547e63049",
                    "LayerId": "9ef152c9-5e38-4498-8ad5-6531fe71fa41"
                },
                {
                    "id": "0ae03b6c-5865-44d4-9b70-9ac66bdfdcda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8a782b2-68c3-470d-919d-41e547e63049",
                    "LayerId": "e33652da-cb1d-4b3d-bf51-0763b61ab56d"
                },
                {
                    "id": "ab314097-65cb-4e22-b75a-3a397b8e8b49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8a782b2-68c3-470d-919d-41e547e63049",
                    "LayerId": "e44be190-5d7d-4c6b-88b5-097c77c4dd24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "9ef152c9-5e38-4498-8ad5-6531fe71fa41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cc42b75-d598-4030-a5f7-b0a6fa0566ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e33652da-cb1d-4b3d-bf51-0763b61ab56d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cc42b75-d598-4030-a5f7-b0a6fa0566ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "6524ba3d-c95c-45f1-b291-124a2e9acba7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cc42b75-d598-4030-a5f7-b0a6fa0566ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "e44be190-5d7d-4c6b-88b5-097c77c4dd24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cc42b75-d598-4030-a5f7-b0a6fa0566ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}