{
    "id": "f80617c6-d435-46c6-a9aa-653500f76e05",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_stars",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 2047,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6bca8c8f-6117-41e7-aa85-24e4883c1d5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f80617c6-d435-46c6-a9aa-653500f76e05",
            "compositeImage": {
                "id": "38e42817-d041-4cfa-9e12-3eafe82cd9ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bca8c8f-6117-41e7-aa85-24e4883c1d5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f89a9ca6-61c5-4bec-b003-c8879fa9e876",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bca8c8f-6117-41e7-aa85-24e4883c1d5d",
                    "LayerId": "dfe8d933-9136-4c43-b491-6b5f33f510d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "dfe8d933-9136-4c43-b491-6b5f33f510d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f80617c6-d435-46c6-a9aa-653500f76e05",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2048,
    "xorig": 0,
    "yorig": 0
}