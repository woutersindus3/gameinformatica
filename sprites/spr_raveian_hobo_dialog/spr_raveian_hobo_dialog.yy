{
    "id": "faeeed21-1b5d-46d4-ab07-715fc520b348",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_raveian_hobo_dialog",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 3,
    "bbox_right": 120,
    "bbox_top": 31,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd50dbca-fd13-43b7-91b9-0e764f239533",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "faeeed21-1b5d-46d4-ab07-715fc520b348",
            "compositeImage": {
                "id": "f3ab24b6-4154-4632-b822-a95f5cef1784",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd50dbca-fd13-43b7-91b9-0e764f239533",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93f94f91-88c6-446b-919d-47e5aff6f33f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd50dbca-fd13-43b7-91b9-0e764f239533",
                    "LayerId": "c86c677e-646b-4328-b093-c92740dae3ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "c86c677e-646b-4328-b093-c92740dae3ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "faeeed21-1b5d-46d4-ab07-715fc520b348",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}