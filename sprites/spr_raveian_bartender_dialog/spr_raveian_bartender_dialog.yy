{
    "id": "399929c8-f3e7-4cd4-b502-b1524644471c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_raveian_bartender_dialog",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 5,
    "bbox_right": 122,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7117e4d0-a498-469b-a3b9-c1208dfc6ceb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "399929c8-f3e7-4cd4-b502-b1524644471c",
            "compositeImage": {
                "id": "d18d8079-f21b-4448-9db5-05afaa4844db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7117e4d0-a498-469b-a3b9-c1208dfc6ceb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f85a952-9b0c-47c9-9cf0-ea2bec76b03d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7117e4d0-a498-469b-a3b9-c1208dfc6ceb",
                    "LayerId": "c1dc01e0-e6ac-49c7-8c10-e5895600378a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "c1dc01e0-e6ac-49c7-8c10-e5895600378a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "399929c8-f3e7-4cd4-b502-b1524644471c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}