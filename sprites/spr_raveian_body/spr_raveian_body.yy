{
    "id": "a44adee0-efc3-4072-bf34-c3c3cd89323e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_raveian_body",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d1fd2f1-1fca-4ef4-9d08-cc87aa7ba86e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a44adee0-efc3-4072-bf34-c3c3cd89323e",
            "compositeImage": {
                "id": "69834a6b-2678-4201-9c1d-023c092bf5aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d1fd2f1-1fca-4ef4-9d08-cc87aa7ba86e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2b0019f-9704-448b-9bfe-ed8b45c0483f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d1fd2f1-1fca-4ef4-9d08-cc87aa7ba86e",
                    "LayerId": "93bdd7a4-f78e-4ce2-b4b2-48e140bd58d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "93bdd7a4-f78e-4ce2-b4b2-48e140bd58d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a44adee0-efc3-4072-bf34-c3c3cd89323e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 5,
    "yorig": 14
}