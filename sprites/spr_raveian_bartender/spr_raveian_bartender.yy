{
    "id": "2cce77cd-1c18-4e69-96f7-7b09f862956b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_raveian_bartender",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b8a5702-61ff-42c3-8bed-8e09f096de50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cce77cd-1c18-4e69-96f7-7b09f862956b",
            "compositeImage": {
                "id": "c12d08b4-9cc7-4201-81d1-6608d425ff5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b8a5702-61ff-42c3-8bed-8e09f096de50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ff81163-d48f-433a-a89c-512c8a54a553",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b8a5702-61ff-42c3-8bed-8e09f096de50",
                    "LayerId": "46ec0024-f309-4b66-9f48-3c2637ab815d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "46ec0024-f309-4b66-9f48-3c2637ab815d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2cce77cd-1c18-4e69-96f7-7b09f862956b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 17
}