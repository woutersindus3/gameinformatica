{
    "id": "e1404217-a9ab-4eef-9163-eb0f740ab0ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ufo_broke",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 38,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0135dc80-2324-4156-b141-c376b6c42f13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1404217-a9ab-4eef-9163-eb0f740ab0ee",
            "compositeImage": {
                "id": "c5a6f215-3554-4624-b0b0-7a25d46c37db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0135dc80-2324-4156-b141-c376b6c42f13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b67bff87-e27c-44a4-93bb-a0b7c9da46c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0135dc80-2324-4156-b141-c376b6c42f13",
                    "LayerId": "dd5fd2e8-1ebc-4e28-860b-9e75a1c84f93"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "dd5fd2e8-1ebc-4e28-860b-9e75a1c84f93",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1404217-a9ab-4eef-9163-eb0f740ab0ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 39,
    "xorig": 19,
    "yorig": 8
}