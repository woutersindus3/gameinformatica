{
    "id": "3f11eb99-ee65-4eb5-a2a1-3b2fde6928d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_text",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b76513f-1e6a-4e1f-8d36-39779df70f10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f11eb99-ee65-4eb5-a2a1-3b2fde6928d9",
            "compositeImage": {
                "id": "8d64f13e-115a-4544-8bd1-d5f93c6b8604",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b76513f-1e6a-4e1f-8d36-39779df70f10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "921202ee-7a7f-4e50-b420-2ae44b99b38d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b76513f-1e6a-4e1f-8d36-39779df70f10",
                    "LayerId": "353215a6-2359-432b-aa32-0080810fb4e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "353215a6-2359-432b-aa32-0080810fb4e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f11eb99-ee65-4eb5-a2a1-3b2fde6928d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 16,
    "yorig": 19
}