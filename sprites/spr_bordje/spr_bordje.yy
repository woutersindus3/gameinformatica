{
    "id": "5aedc3d1-8ab6-407a-89f2-e140dc053136",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bordje",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eaf4e2c9-d3c8-49e2-8ff3-609619d135c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aedc3d1-8ab6-407a-89f2-e140dc053136",
            "compositeImage": {
                "id": "21771e95-a91b-4d56-96ea-2b651cb88622",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaf4e2c9-d3c8-49e2-8ff3-609619d135c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07d2bf4c-6889-451f-a009-4ebbed4f7dc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaf4e2c9-d3c8-49e2-8ff3-609619d135c5",
                    "LayerId": "cd9ab2fc-7793-4828-9ef9-fc2153bb2f37"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "cd9ab2fc-7793-4828-9ef9-fc2153bb2f37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5aedc3d1-8ab6-407a-89f2-e140dc053136",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}