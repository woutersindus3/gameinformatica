{
    "id": "74a14d2c-4fcc-4b1e-a391-d0c95cc80e8f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventory",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 116,
    "bbox_left": 0,
    "bbox_right": 207,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3cb2b0f8-38ef-48b5-9e2b-d5cb0a0527ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74a14d2c-4fcc-4b1e-a391-d0c95cc80e8f",
            "compositeImage": {
                "id": "b9830c89-b86a-4087-8d52-e0b20800381b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cb2b0f8-38ef-48b5-9e2b-d5cb0a0527ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2ad92da-1b3b-42d1-8bd9-bb35141d9d07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cb2b0f8-38ef-48b5-9e2b-d5cb0a0527ed",
                    "LayerId": "978dc82c-06b3-4da1-8f1c-c95ad4fdbce1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 117,
    "layers": [
        {
            "id": "978dc82c-06b3-4da1-8f1c-c95ad4fdbce1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74a14d2c-4fcc-4b1e-a391-d0c95cc80e8f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 208,
    "xorig": 0,
    "yorig": 0
}