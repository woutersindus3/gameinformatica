{
    "id": "49132c8c-f539-4910-bf57-5c3e09d46be8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 279,
    "bbox_left": 28,
    "bbox_right": 639,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d50a6f41-294a-4736-9104-ae35179df16e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49132c8c-f539-4910-bf57-5c3e09d46be8",
            "compositeImage": {
                "id": "11e6a1c8-cf4d-4e24-ba81-2bc054b970cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d50a6f41-294a-4736-9104-ae35179df16e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f83a9997-8e4f-40c8-9696-e00fdef22195",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d50a6f41-294a-4736-9104-ae35179df16e",
                    "LayerId": "ff1b0720-a976-416c-b566-467efbd33570"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "ff1b0720-a976-416c-b566-467efbd33570",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49132c8c-f539-4910-bf57-5c3e09d46be8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}