{
    "id": "9af9a9ce-f161-4f3c-8228-cb8fe9ed3fcb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_namebox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9e1ac48-da5d-439c-8b86-6df1d8e1b735",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9af9a9ce-f161-4f3c-8228-cb8fe9ed3fcb",
            "compositeImage": {
                "id": "f08226a6-786b-4a58-871c-a1aedaf017f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9e1ac48-da5d-439c-8b86-6df1d8e1b735",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4856f71a-31ec-4d71-888a-eae675ca0ac6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9e1ac48-da5d-439c-8b86-6df1d8e1b735",
                    "LayerId": "01db733b-20f0-45ba-b0ed-065d1dbc7aa0"
                },
                {
                    "id": "aa5941db-4b9f-4a7a-8fc3-b53f3286d787",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9e1ac48-da5d-439c-8b86-6df1d8e1b735",
                    "LayerId": "b5727295-51d2-4ce2-a98a-bc7866a4bbc4"
                },
                {
                    "id": "844739ff-42b5-4fef-a152-193474a0a463",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9e1ac48-da5d-439c-8b86-6df1d8e1b735",
                    "LayerId": "99175650-0bb7-4e5f-b5d0-e6255af3b9b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "01db733b-20f0-45ba-b0ed-065d1dbc7aa0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9af9a9ce-f161-4f3c-8228-cb8fe9ed3fcb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b5727295-51d2-4ce2-a98a-bc7866a4bbc4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9af9a9ce-f161-4f3c-8228-cb8fe9ed3fcb",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "99175650-0bb7-4e5f-b5d0-e6255af3b9b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9af9a9ce-f161-4f3c-8228-cb8fe9ed3fcb",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}