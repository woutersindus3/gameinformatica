{
    "id": "df9ef8d2-684b-4dba-8f34-c9d4db36b922",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ravian",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 3,
    "bbox_right": 121,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb0a3be5-81b7-48ca-9145-bd00e0edb440",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df9ef8d2-684b-4dba-8f34-c9d4db36b922",
            "compositeImage": {
                "id": "8922b50a-e0d3-46ec-ba0e-5e4f8d849d4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb0a3be5-81b7-48ca-9145-bd00e0edb440",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ff1d958-a1fc-4a74-af9d-54f37d994344",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb0a3be5-81b7-48ca-9145-bd00e0edb440",
                    "LayerId": "d50a6288-78c1-4539-822d-c1a5d528588a"
                }
            ]
        },
        {
            "id": "ee776150-545d-42fc-8372-534fdee71fb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df9ef8d2-684b-4dba-8f34-c9d4db36b922",
            "compositeImage": {
                "id": "ea4642af-3858-4507-b579-6ad899272eac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee776150-545d-42fc-8372-534fdee71fb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "553f6f67-c20f-4b59-8dc3-ec1210bfb9d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee776150-545d-42fc-8372-534fdee71fb2",
                    "LayerId": "d50a6288-78c1-4539-822d-c1a5d528588a"
                }
            ]
        },
        {
            "id": "e5306745-b0eb-4219-a33c-37505abb2a4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df9ef8d2-684b-4dba-8f34-c9d4db36b922",
            "compositeImage": {
                "id": "e648c22f-60b0-4681-bb6e-e822e294c741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5306745-b0eb-4219-a33c-37505abb2a4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0e3738a-74ed-4a5f-a589-2c915cea3b5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5306745-b0eb-4219-a33c-37505abb2a4a",
                    "LayerId": "d50a6288-78c1-4539-822d-c1a5d528588a"
                }
            ]
        },
        {
            "id": "648c8548-6157-4457-b587-ac5cae4a19d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df9ef8d2-684b-4dba-8f34-c9d4db36b922",
            "compositeImage": {
                "id": "9cb2d211-1e54-487e-928e-bc0dd34e7e0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "648c8548-6157-4457-b587-ac5cae4a19d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbc5dc27-3c31-406f-88c0-9edfaa964174",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "648c8548-6157-4457-b587-ac5cae4a19d9",
                    "LayerId": "d50a6288-78c1-4539-822d-c1a5d528588a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d50a6288-78c1-4539-822d-c1a5d528588a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df9ef8d2-684b-4dba-8f34-c9d4db36b922",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}