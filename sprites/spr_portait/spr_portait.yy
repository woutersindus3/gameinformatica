{
    "id": "1fa6d5a9-472d-472c-82b1-61ece6ea6a2d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portait",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3348f059-e093-4ecf-be36-8b9726ae5b03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fa6d5a9-472d-472c-82b1-61ece6ea6a2d",
            "compositeImage": {
                "id": "ad478a24-a910-499e-89f1-61e2a36401e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3348f059-e093-4ecf-be36-8b9726ae5b03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c71c0e4-e91e-4cab-a26b-91441c2cb879",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3348f059-e093-4ecf-be36-8b9726ae5b03",
                    "LayerId": "781a9be2-fceb-4440-8f6b-d05d367d0ed7"
                },
                {
                    "id": "64540ef7-ba8a-488a-ac82-f96a2e64bfef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3348f059-e093-4ecf-be36-8b9726ae5b03",
                    "LayerId": "e8096813-b809-4c88-b54f-f013c9f70a86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "781a9be2-fceb-4440-8f6b-d05d367d0ed7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fa6d5a9-472d-472c-82b1-61ece6ea6a2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e8096813-b809-4c88-b54f-f013c9f70a86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fa6d5a9-472d-472c-82b1-61ece6ea6a2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}