{
    "id": "9026b286-c72d-477c-9028-53b77fcf1cd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cutscene_balk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d0d7f11-f65b-4601-990f-f0e3064723ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9026b286-c72d-477c-9028-53b77fcf1cd2",
            "compositeImage": {
                "id": "ab826f24-f159-4276-80d4-1931fa889be6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d0d7f11-f65b-4601-990f-f0e3064723ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62ccf1be-39e1-4f08-ac81-6c3cdd6aa7d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d0d7f11-f65b-4601-990f-f0e3064723ee",
                    "LayerId": "74eea31a-5fd6-4b52-959e-abd50a5f9ad7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "74eea31a-5fd6-4b52-959e-abd50a5f9ad7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9026b286-c72d-477c-9028-53b77fcf1cd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}