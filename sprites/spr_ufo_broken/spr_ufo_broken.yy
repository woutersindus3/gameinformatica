{
    "id": "7c5b62da-eb9d-46a4-8147-50975b5186e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ufo_broken",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 15,
    "bbox_right": 50,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6cb8eb9-062f-4d6c-bda9-3dc0266bf91d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c5b62da-eb9d-46a4-8147-50975b5186e2",
            "compositeImage": {
                "id": "d963eb81-83fa-496b-a4e5-37dede38415c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6cb8eb9-062f-4d6c-bda9-3dc0266bf91d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84a69568-f7e6-46ae-8c5e-c1d2f166fe61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6cb8eb9-062f-4d6c-bda9-3dc0266bf91d",
                    "LayerId": "eae3ff99-1b74-4482-83d2-9f3fd97012f2"
                }
            ]
        },
        {
            "id": "7c0c6a5c-9b09-4966-baa3-377b8be4c761",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c5b62da-eb9d-46a4-8147-50975b5186e2",
            "compositeImage": {
                "id": "612468be-2230-4f9e-baea-af3cb3ff6e07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c0c6a5c-9b09-4966-baa3-377b8be4c761",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "208c9453-fc6a-4124-aa38-57ce0b423ed9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c0c6a5c-9b09-4966-baa3-377b8be4c761",
                    "LayerId": "eae3ff99-1b74-4482-83d2-9f3fd97012f2"
                }
            ]
        },
        {
            "id": "cfab1a68-74b9-44ca-b566-4eab4d2acb81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c5b62da-eb9d-46a4-8147-50975b5186e2",
            "compositeImage": {
                "id": "16913ef3-19ae-45d9-b886-4f4596ec9065",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfab1a68-74b9-44ca-b566-4eab4d2acb81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e13e810e-440c-4bc0-95fa-8e6c5f835cc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfab1a68-74b9-44ca-b566-4eab4d2acb81",
                    "LayerId": "eae3ff99-1b74-4482-83d2-9f3fd97012f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "eae3ff99-1b74-4482-83d2-9f3fd97012f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c5b62da-eb9d-46a4-8147-50975b5186e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}