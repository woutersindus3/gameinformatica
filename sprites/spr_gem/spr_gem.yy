{
    "id": "756874fb-ce3d-4c7a-bb24-8b0cc19ed8b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gem",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71040514-d33f-47ed-a0b8-4768d4dc6ee0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "756874fb-ce3d-4c7a-bb24-8b0cc19ed8b6",
            "compositeImage": {
                "id": "b6c92abe-ce63-4990-bde1-a09b701e5c32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71040514-d33f-47ed-a0b8-4768d4dc6ee0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "839fd6de-6a96-4840-a632-887a232b2e0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71040514-d33f-47ed-a0b8-4768d4dc6ee0",
                    "LayerId": "517bdd28-fe0f-4f7a-8e59-de8cae32e512"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "517bdd28-fe0f-4f7a-8e59-de8cae32e512",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "756874fb-ce3d-4c7a-bb24-8b0cc19ed8b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}