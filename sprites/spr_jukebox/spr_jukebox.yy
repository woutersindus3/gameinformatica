{
    "id": "ec00a276-f748-45d1-8500-b2bd769cfbff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jukebox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 19,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3a8b817-db83-4bbb-834c-019f437f59ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec00a276-f748-45d1-8500-b2bd769cfbff",
            "compositeImage": {
                "id": "55432190-4eb5-4ad3-b3d8-7db4caaeb6a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3a8b817-db83-4bbb-834c-019f437f59ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3985b917-cf84-4c95-b517-46ea3116db4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3a8b817-db83-4bbb-834c-019f437f59ab",
                    "LayerId": "695d2953-ea05-4122-9b49-99752933eec1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "695d2953-ea05-4122-9b49-99752933eec1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec00a276-f748-45d1-8500-b2bd769cfbff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 12,
    "yorig": 31
}