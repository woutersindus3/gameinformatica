{
    "id": "232837b5-86b6-4324-98c6-b0beea6d1e67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 1,
    "bbox_right": 9,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d54736d-f0da-41cd-a521-63c079634488",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "232837b5-86b6-4324-98c6-b0beea6d1e67",
            "compositeImage": {
                "id": "bbea5056-7b4a-448c-9a8a-927b07268234",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d54736d-f0da-41cd-a521-63c079634488",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "667cc05d-800b-430e-ae34-582c7a9366b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d54736d-f0da-41cd-a521-63c079634488",
                    "LayerId": "08b8855b-1abe-4544-a540-4173d914aeb2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "08b8855b-1abe-4544-a540-4173d914aeb2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "232837b5-86b6-4324-98c6-b0beea6d1e67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}