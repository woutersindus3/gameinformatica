{
    "id": "0f3ce5a7-4831-4d02-8f60-343793b711d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_raveian_hobo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d468a2b7-2fd7-4808-8d12-28618ae65206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f3ce5a7-4831-4d02-8f60-343793b711d3",
            "compositeImage": {
                "id": "de8c3e29-3bd3-41b5-8e1a-ec957c7e027e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d468a2b7-2fd7-4808-8d12-28618ae65206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5d0b0ff-3bb2-4c7b-bb40-e8ff286ff015",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d468a2b7-2fd7-4808-8d12-28618ae65206",
                    "LayerId": "9b36aa1e-9b7f-4d73-9adc-c3ccdc039bb5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "9b36aa1e-9b7f-4d73-9adc-c3ccdc039bb5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f3ce5a7-4831-4d02-8f60-343793b711d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 17
}