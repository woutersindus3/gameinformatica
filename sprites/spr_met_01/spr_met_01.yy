{
    "id": "3b054090-f66a-4f87-acf7-c709d576b711",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_met_01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 15,
    "bbox_right": 45,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a03d8c1-3663-40ad-95cb-db8042dd2149",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b054090-f66a-4f87-acf7-c709d576b711",
            "compositeImage": {
                "id": "44cc5776-d014-48d1-9a1e-537c7a2c97b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a03d8c1-3663-40ad-95cb-db8042dd2149",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bd7beab-9f38-43d9-9767-6e6d85a1d430",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a03d8c1-3663-40ad-95cb-db8042dd2149",
                    "LayerId": "01e8e985-b519-4dd8-ada9-4e0536bdf0b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "01e8e985-b519-4dd8-ada9-4e0536bdf0b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b054090-f66a-4f87-acf7-c709d576b711",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}