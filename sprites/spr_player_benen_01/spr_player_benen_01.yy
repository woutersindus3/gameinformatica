{
    "id": "b4909754-86b4-4551-86b8-997ae639f1f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_benen_01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 3,
    "bbox_right": 81,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9442a1e9-16b1-4801-9ad5-8da997b4593a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4909754-86b4-4551-86b8-997ae639f1f6",
            "compositeImage": {
                "id": "1976f85e-ce70-4a97-bcbe-348f1a62cd98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9442a1e9-16b1-4801-9ad5-8da997b4593a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a42e99c-5bcb-446b-b006-44f084dfe579",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9442a1e9-16b1-4801-9ad5-8da997b4593a",
                    "LayerId": "97a6bac7-b7e1-45cf-aac2-509497c1d069"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 84,
    "layers": [
        {
            "id": "97a6bac7-b7e1-45cf-aac2-509497c1d069",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4909754-86b4-4551-86b8-997ae639f1f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 0,
    "yorig": 0
}