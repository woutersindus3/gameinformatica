{
    "id": "790d567b-4379-42d9-99ee-36fee6308b2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tileset_planet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b15c5dae-faa4-49e6-8ee5-04bd7ee1941d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "790d567b-4379-42d9-99ee-36fee6308b2f",
            "compositeImage": {
                "id": "a42231a2-57ac-4325-9ad8-4c459add3160",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b15c5dae-faa4-49e6-8ee5-04bd7ee1941d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fb7c04b-e5a3-48ee-9bfb-4d02923fd009",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b15c5dae-faa4-49e6-8ee5-04bd7ee1941d",
                    "LayerId": "06949350-6086-41c1-873a-8dcfb7ea5bfb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 176,
    "layers": [
        {
            "id": "06949350-6086-41c1-873a-8dcfb7ea5bfb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "790d567b-4379-42d9-99ee-36fee6308b2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 67,
    "yorig": 9
}