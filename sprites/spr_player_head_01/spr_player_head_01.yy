{
    "id": "6978fd3f-04f5-4f2e-8d17-6e32d1726daa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_head_01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 2,
    "bbox_right": 81,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3050195-024d-4fc8-ab0f-6401a7b54924",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6978fd3f-04f5-4f2e-8d17-6e32d1726daa",
            "compositeImage": {
                "id": "3f0c1f78-9454-41b7-aacb-601f21ea025c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3050195-024d-4fc8-ab0f-6401a7b54924",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96473324-0b4f-4190-8c56-ac570fd05196",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3050195-024d-4fc8-ab0f-6401a7b54924",
                    "LayerId": "42a25416-bcb9-44e9-9887-520c756b47ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 84,
    "layers": [
        {
            "id": "42a25416-bcb9-44e9-9887-520c756b47ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6978fd3f-04f5-4f2e-8d17-6e32d1726daa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": -41,
    "yorig": 16
}