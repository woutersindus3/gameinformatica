{
    "id": "7e766892-22a2-430d-9845-61f2b2e7774d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_motor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 2,
    "bbox_right": 27,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc2d7809-1661-4f89-9dfb-b9763ea51cbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e766892-22a2-430d-9845-61f2b2e7774d",
            "compositeImage": {
                "id": "3e7e6cf1-910c-495b-baf6-232789fe802e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc2d7809-1661-4f89-9dfb-b9763ea51cbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "443d3909-a978-4aa7-8e79-f4aac94b5881",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc2d7809-1661-4f89-9dfb-b9763ea51cbd",
                    "LayerId": "ad8de618-603e-4d5f-8a95-5dfc2754465a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ad8de618-603e-4d5f-8a95-5dfc2754465a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e766892-22a2-430d-9845-61f2b2e7774d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}