{
    "id": "fcd0135f-4461-46bc-8856-fed63e415d13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_table",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2511e561-3a8c-4ee9-81cf-413ef08a582a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fcd0135f-4461-46bc-8856-fed63e415d13",
            "compositeImage": {
                "id": "f9f4af46-c7a0-4a91-9fbe-c1b531ecdca7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2511e561-3a8c-4ee9-81cf-413ef08a582a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b6663ab-67e1-46a3-bc43-d247fe9df807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2511e561-3a8c-4ee9-81cf-413ef08a582a",
                    "LayerId": "dde7c343-6fcc-440d-8726-5d8fd5bfa67d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dde7c343-6fcc-440d-8726-5d8fd5bfa67d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fcd0135f-4461-46bc-8856-fed63e415d13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}