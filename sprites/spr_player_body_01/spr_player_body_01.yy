{
    "id": "1cc44c72-e7a5-4d9f-82d6-ab7ff9efbc1c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_body_01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 77,
    "bbox_left": 2,
    "bbox_right": 81,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78392107-5d50-4390-8e4c-8f5f206053c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cc44c72-e7a5-4d9f-82d6-ab7ff9efbc1c",
            "compositeImage": {
                "id": "811c7206-7656-4a09-a3c6-7c074fdaf36b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78392107-5d50-4390-8e4c-8f5f206053c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a340be5f-9e4b-43d0-9848-4d247f241cd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78392107-5d50-4390-8e4c-8f5f206053c7",
                    "LayerId": "03c571c2-379b-4525-ae81-d89c4c65458d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 84,
    "layers": [
        {
            "id": "03c571c2-379b-4525-ae81-d89c4c65458d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1cc44c72-e7a5-4d9f-82d6-ab7ff9efbc1c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 0,
    "yorig": 0
}