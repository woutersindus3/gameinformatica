{
    "id": "72814a4f-e141-4397-b186-53a74453ca98",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_null",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "712c565b-8390-44e9-a620-f0368e7bc8f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72814a4f-e141-4397-b186-53a74453ca98",
            "compositeImage": {
                "id": "3763ac77-92b5-4dde-904a-95934e428c29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "712c565b-8390-44e9-a620-f0368e7bc8f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "483479fe-b594-4a0f-aa13-b4213fda0bbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "712c565b-8390-44e9-a620-f0368e7bc8f5",
                    "LayerId": "0cdb0bc5-2cbf-4ea0-be93-03106a550c83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "0cdb0bc5-2cbf-4ea0-be93-03106a550c83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72814a4f-e141-4397-b186-53a74453ca98",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}