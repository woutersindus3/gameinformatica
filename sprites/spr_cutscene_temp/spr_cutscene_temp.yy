{
    "id": "e6d0304d-258b-4c12-939a-64cab0435707",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cutscene_temp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "67016d3d-fc62-4326-b636-5da73b3a7462",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6d0304d-258b-4c12-939a-64cab0435707",
            "compositeImage": {
                "id": "8f9257bd-6916-4a3f-808c-41c9a0f1482b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67016d3d-fc62-4326-b636-5da73b3a7462",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc00a1e3-7363-4c02-ba1a-d9941f81b522",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67016d3d-fc62-4326-b636-5da73b3a7462",
                    "LayerId": "403dcd78-6f94-44dd-b68b-08e90b246063"
                },
                {
                    "id": "8ec67168-e51e-4dcc-bf28-c545b81f3251",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67016d3d-fc62-4326-b636-5da73b3a7462",
                    "LayerId": "1cf9d87d-5317-4938-943c-b5477ac9db0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "1cf9d87d-5317-4938-943c-b5477ac9db0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6d0304d-258b-4c12-939a-64cab0435707",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "403dcd78-6f94-44dd-b68b-08e90b246063",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6d0304d-258b-4c12-939a-64cab0435707",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}