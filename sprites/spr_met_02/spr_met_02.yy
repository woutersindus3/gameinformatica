{
    "id": "c287174f-2497-40d6-a801-135b2546c7c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_met_02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 16,
    "bbox_right": 46,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80078aa4-5201-4262-8c9f-130cd399090d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c287174f-2497-40d6-a801-135b2546c7c2",
            "compositeImage": {
                "id": "29e89d06-c208-49e6-91ad-750dfb009412",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80078aa4-5201-4262-8c9f-130cd399090d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bce8974-4d95-4cc6-825d-9c06e5c444b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80078aa4-5201-4262-8c9f-130cd399090d",
                    "LayerId": "bb727ed4-4d99-47a7-972b-21a175518548"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bb727ed4-4d99-47a7-972b-21a175518548",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c287174f-2497-40d6-a801-135b2546c7c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}