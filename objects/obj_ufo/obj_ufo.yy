{
    "id": "7fde944f-8449-4dd6-8268-2637908a69c1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ufo",
    "eventList": [
        {
            "id": "2e40b02a-b56d-43f5-ad0f-f28d9ac3e333",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7fde944f-8449-4dd6-8268-2637908a69c1"
        },
        {
            "id": "b6f377f0-9d3b-4de0-b0bc-09c7ff84981c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7fde944f-8449-4dd6-8268-2637908a69c1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "686f7d49-1d6c-4291-b584-b130e83bd5a3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "26ca9e80-41cf-42ba-893a-5233df551fbe",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "8bcac813-4e6f-4711-8e78-520f3cf8f297",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 16
        },
        {
            "id": "b381ec06-a59d-4572-826e-479c60fba633",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "440f00aa-4234-41c3-9878-7e1fbc71dbda",
    "visible": true
}