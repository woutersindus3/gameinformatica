/// @description Insert description here
// You can write your code in this editor


//Keyboard
if(global.ready == true){	
	xaxis = (keyboard_check(ord("D")) - keyboard_check(ord("A")) ) 
	yaxis = (keyboard_check(ord("S")) - keyboard_check(ord("W")) ) 
}

//Moving
dir = point_direction(0,0,xaxis,yaxis);

if(xaxis == 0 and yaxis == 0){
	len = 0;
}else{
	len = spd;	
}

hspd = lengthdir_x(len,dir);
vspd = lengthdir_y(len,dir);

//Colision
if(place_meeting(x+hspd,y,obj_met) or place_meeting(x+hspd,y,obj_met_giant)){
	scr_slide_transition(TRANS_MODE.GOTO, rm_crash);
}

if(place_meeting(x,y+vspd,obj_met) or place_meeting(x,y+vspd,obj_met_giant)){
	scr_slide_transition(TRANS_MODE.GOTO, rm_crash);
}

//Movement
phy_position_x += hspd;
phy_position_y += vspd;
phy_position_x = clamp(phy_position_x,0,room_width);
phy_position_y = clamp(phy_position_y,0,room_height);



