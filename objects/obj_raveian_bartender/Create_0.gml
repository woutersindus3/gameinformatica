event_inherited();

questtext = [

	["I am so depressed, since i started this pub noone has came to buy a beer, it would be amazing if someone came to buy a beer."],
	["I need the money fast. My wife is going to kill me if I am not making a single penny."],
	["Finally someone to sell a beer to. Enjoy dude!"]
	
]

name = "Gruckie"
portrait = spr_raveian_bartender_dialog;
voice = snd_gruckie;

hasquest = true;

min_dis = 32;

quest = "quest_coin_started";
questcondition = "quest_coin_got";	 
questfinished = "quest_coin_finished";
