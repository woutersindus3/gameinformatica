{
    "id": "d5162cb3-4d17-4c85-a721-886516379ef8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_raveian_bartender",
    "eventList": [
        {
            "id": "aeaa0b2d-0912-4fb5-81cf-fae03fc1ea4a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d5162cb3-4d17-4c85-a721-886516379ef8"
        },
        {
            "id": "0055390d-a344-43cd-aa1e-9671f7c688c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d5162cb3-4d17-4c85-a721-886516379ef8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "af372286-8008-4f92-bb86-d3369ce6179c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2cce77cd-1c18-4e69-96f7-7b09f862956b",
    "visible": true
}