{
    "id": "420a6f88-ebea-4983-85b6-1a3707e8bd48",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_jukebox",
    "eventList": [
        {
            "id": "fed5e372-5ce6-42df-9250-dc2b4d0b0965",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "420a6f88-ebea-4983-85b6-1a3707e8bd48"
        },
        {
            "id": "be2ac451-970d-46c7-9c1d-14b4ee64cdd8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "420a6f88-ebea-4983-85b6-1a3707e8bd48"
        },
        {
            "id": "52522fea-5192-42d6-976c-2cfc90776f34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "420a6f88-ebea-4983-85b6-1a3707e8bd48"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fedaa4c1-45fb-4eaa-b3ff-c510d8518d3f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ec00a276-f748-45d1-8500-b2bd769cfbff",
    "visible": false
}