event_inherited();

if(obj_game.switches[? "quest_coin_started"] == true && obj_game.switches[? "quest_coin_got"] == false){
	active = true;	
}


//Collision
if(place_meeting(x,y,obj_player) && active){	
	obj_game.switches[? "quest_coin_got"] = true
	
	text = [

	"Bonk! That hurt. But there is a coin inside. Just what I needed."
	
	]
	
	if(!instance_exists(obj_dialoog)){
		var inst = instance_create_layer(0,0,"Dialog", obj_dialoog);
		inst.text = text;
		inst.name = name;
		inst.portrait = portrait;
		inst.text_wrapped = scr_wrap(inst.text[inst.page],inst.text_max_width);
		inst.str_len = string_length(inst.text_wrapped);
	}
	
	active = false;
	
		text = [

		"There is nothing in there, just some dusty old LP's."
	
	]
	
}