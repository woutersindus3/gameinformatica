{
    "id": "cf4126bb-293a-4cae-915b-0802441d71a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_met",
    "eventList": [
        {
            "id": "e52509c6-6a22-4569-aa4b-87583b302935",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cf4126bb-293a-4cae-915b-0802441d71a5"
        },
        {
            "id": "987896b2-4e90-4c3d-839d-d3760cd4a2e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cf4126bb-293a-4cae-915b-0802441d71a5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "008c4cc7-fd7a-4198-bda7-682e5db46a15",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "66b21826-902c-4310-8af0-42a18f7b0b15",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "6c739b46-e472-4c8e-91b6-da1531798e77",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "deb6ce0c-d05c-45df-a381-5a0141b1634c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3b054090-f66a-4f87-acf7-c709d576b711",
    "visible": true
}