event_inherited();

questtext = [

	["I need to find my lost LP, it has to be somewhere close. It goes something like Y, M, C. You know what I mean. Can you help me?"],
	["Have you already found my LP, its very worthy. Its a special edition!"],
	["You found it! How can i thank you, I only have this wierd crystal, you cane heve it if you want to."]
	
]

name = "Bart"
portrait = spr_ravian;
voice = snd_bart;

hasquest = true;

quest = "quest_lp_started";
questcondition = "quest_lp_got";	 
questfinished = "quest_lp_finished";
