{
    "id": "d619589e-afe0-4d46-a37f-e90056f28d9a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ufo_broken",
    "eventList": [
        {
            "id": "09487f7f-5eff-4dc9-aeed-d4d2b0aa92d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d619589e-afe0-4d46-a37f-e90056f28d9a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "9dd9797e-23cd-44b8-9343-9fdca9644fa9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "a636199e-5966-4bbc-b4a2-49c665ad993d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "c6c55430-3e45-46d8-9fe5-a4ec764fe44f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "f916d3e9-cabe-44ab-8e7a-628c51f09ff0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7c5b62da-eb9d-46a4-8147-50975b5186e2",
    "visible": true
}