depth = -1;
scale = 6.16;
show_inventory = false;

gui_width = display_get_gui_width();
gui_height = display_get_gui_height();

inv_width = 208;
inv_height = 117;

spr_inventory_ui = spr_inventory;

inv_x = (gui_width * .5) - (inv_width * .5 * scale);	 
inv_y = (gui_height * .5) - (inv_height * .5 * scale);	

motor = false;
crystal = false;

m_x = 134 * scale;
m_y = 40 * scale;

g_x = 40 * scale;
g_y = 40 * scale;
