//Draw inventory back
if(!show_inventory) exit;

draw_sprite_ext(spr_inventory_ui,0,inv_x,inv_y,scale,scale,0,c_white,1);

if(motor){
	
	draw_sprite_ext(spr_motor,0,m_x,m_y,scale,scale,0,c_white,1);	
	
} else {

	draw_sprite_ext(spr_motor,0,m_x,m_y,scale,scale,0,c_white,.2);

}

if(crystal){

	draw_sprite_ext(spr_gem,0,g_x,g_y,scale,scale,0,c_white,1);

} else {
	
	draw_sprite_ext(spr_gem,0,g_x,g_y,scale,scale,0,c_white,.2);
	
}