var dis = point_distance(obj_player.x,obj_player.y,x,y);

if(dis < min_dis && keyboard_check_pressed(ord("E"))){
	
	if(hasquest){	
		if(obj_game.switches[? questcondition]){qstage = 2; obj_game.switches[? questfinished]	= true;}
		else if(obj_game.switches[? quest]){qstage = 1;}
		else{stage = 0; obj_game.switches[? quest] = true;}		
	}	

	if(!instance_exists(obj_dialoog)){
		var inst = instance_create_layer(0,0,"Dialog", obj_dialoog);
		if(hasquest) {inst.text = questtext[qstage];}
		else inst.text = text;
		inst.name = name;
		inst.portrait = portrait;
		inst.text_wrapped = scr_wrap(inst.text[inst.page],inst.text_max_width);
		inst.str_len = string_length(inst.text_wrapped);
	}
	
	

}