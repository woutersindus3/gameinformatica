{
    "id": "fedaa4c1-45fb-4eaa-b3ff-c510d8518d3f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interact_base",
    "eventList": [
        {
            "id": "0b7a47a7-c9e2-4952-bee6-766d57b1a216",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fedaa4c1-45fb-4eaa-b3ff-c510d8518d3f"
        },
        {
            "id": "f2e6a16b-9af2-420c-851b-8b46efc4b5ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fedaa4c1-45fb-4eaa-b3ff-c510d8518d3f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6d68adfd-9ba6-4548-bdcc-4891b985027d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}