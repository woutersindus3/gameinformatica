{
    "id": "e6b0fca3-a08a-4849-bcbc-fb2e8d652d4a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_stoel",
    "eventList": [
        {
            "id": "76d0cf94-b395-4dd6-b7a5-6af779ec10ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e6b0fca3-a08a-4849-bcbc-fb2e8d652d4a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6d68adfd-9ba6-4548-bdcc-4891b985027d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4f92ab1-cfed-4003-a102-23e880662b36",
    "visible": false
}