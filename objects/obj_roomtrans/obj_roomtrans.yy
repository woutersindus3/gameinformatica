{
    "id": "dee41bbd-7a60-4446-8386-9fb40a6259fb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_roomtrans",
    "eventList": [
        {
            "id": "ffd4912d-d73e-42d7-a5d9-e20184698a59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dee41bbd-7a60-4446-8386-9fb40a6259fb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6d68adfd-9ba6-4548-bdcc-4891b985027d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "656ad89e-513a-44cd-a008-becfcbd311e0",
    "visible": false
}