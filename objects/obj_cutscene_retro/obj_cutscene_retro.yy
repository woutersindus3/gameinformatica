{
    "id": "4ebdd5e3-ba32-4cb4-8d93-7bda184be034",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cutscene_retro",
    "eventList": [
        {
            "id": "588d43fa-352a-4ee8-9256-699d6082026f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4ebdd5e3-ba32-4cb4-8d93-7bda184be034"
        },
        {
            "id": "234b5e90-1ee9-41d0-bb09-07d625a13ed2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4ebdd5e3-ba32-4cb4-8d93-7bda184be034"
        },
        {
            "id": "f3d1b653-7bb9-40cf-9927-8daa390901c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "4ebdd5e3-ba32-4cb4-8d93-7bda184be034"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}