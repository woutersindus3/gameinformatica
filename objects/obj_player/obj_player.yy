{
    "id": "025f70a5-d237-44ca-bba7-17b92aa44863",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "464b5d9c-7c58-4432-b4c2-e959f8ac3250",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "025f70a5-d237-44ca-bba7-17b92aa44863"
        },
        {
            "id": "a23765f6-ef13-4951-b80e-ce52f92ba89e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "025f70a5-d237-44ca-bba7-17b92aa44863"
        },
        {
            "id": "643c95d2-ddfb-46b5-86ac-24cad6d9b876",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "025f70a5-d237-44ca-bba7-17b92aa44863"
        },
        {
            "id": "e463fb70-fa21-49dc-b3a3-d4b180f62281",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "025f70a5-d237-44ca-bba7-17b92aa44863"
        }
    ],
    "maskSpriteId": "191f5cdb-4f1d-449a-bcfd-14113c0f32db",
    "overriddenProperties": null,
    "parentObjectId": "6d68adfd-9ba6-4548-bdcc-4891b985027d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "d9086cdb-20c7-4762-9e4d-c7567861f58d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "69cc180c-60dd-435e-b218-84f909adcc65",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 14,
            "y": 0
        },
        {
            "id": "03f9087e-4990-4246-8986-482443dad0f5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 14,
            "y": 21
        },
        {
            "id": "6a3ec38e-5bc2-4e4b-8031-59d1e1211339",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 21
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}