/// @description Insert description here
// You can write your code in this editor

//--
var anim_length = 6;
var frame_width = 14;
var frame_height = 21;
var anim_speed = 7;

if	   (xaxis < 0) yframe = 3;
else if(xaxis > 0) yframe = 2;
else if(yaxis < 0) yframe = 0;
else if(yaxis > 0) yframe = 1;
else xframe = 0;

var xx = x-x_offset
var yy = y-y_offset

draw_sprite_part(legs_sprite,0,floor(xframe)*frame_width,yframe*frame_height,14,21,xx,yy);
draw_sprite_part(body_sprite,0,floor(xframe)*frame_width,yframe*frame_height,14,21,xx,yy);
draw_sprite_part(head_sprite,0,floor(xframe)*frame_width,yframe*frame_height,14,21,xx,yy);


//plus xframe
if(xframe < anim_length-1){xframe += anim_speed/room_speed;}
else					  {xframe = 0; }

//draw_rectangle_color(bbox_left,bbox_top,bbox_right,bbox_bottom,c_yellow,c_yellow,c_yellow,c_yellow,true);