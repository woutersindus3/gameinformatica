if(!instance_exists(obj_dialoog)){
	var inst = instance_create_layer(0,0,"Dialog", obj_dialoog);
	inst.text = text;
	inst.name = name;
	inst.portrait = portrait;
	inst.text_wrapped = scr_wrap(inst.text[inst.page],inst.text_max_width);
	inst.str_len = string_length(inst.text_wrapped);
} 