{
    "id": "655e6ec9-db67-4bde-a07d-e2445d9acc18",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interact",
    "eventList": [
        {
            "id": "f1f0c2a3-0443-49b6-830c-c870b516d353",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "655e6ec9-db67-4bde-a07d-e2445d9acc18"
        },
        {
            "id": "82dd80a7-a207-44e9-a953-7a66bc8a0700",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "655e6ec9-db67-4bde-a07d-e2445d9acc18"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3f11eb99-ee65-4eb5-a2a1-3b2fde6928d9",
    "visible": false
}