{
    "id": "2212ecd0-9708-4bdc-bd42-769019c31d5f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lp",
    "eventList": [
        {
            "id": "97f283f2-2a92-44b6-a943-89e2d4ce2fc2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2212ecd0-9708-4bdc-bd42-769019c31d5f"
        },
        {
            "id": "a3313a37-51ca-42a3-990a-4ede62b6b733",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2212ecd0-9708-4bdc-bd42-769019c31d5f"
        },
        {
            "id": "e667e0ed-7ba3-41d4-a682-a5495607a2bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2212ecd0-9708-4bdc-bd42-769019c31d5f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fedaa4c1-45fb-4eaa-b3ff-c510d8518d3f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "232837b5-86b6-4324-98c6-b0beea6d1e67",
    "visible": false
}