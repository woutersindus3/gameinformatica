event_inherited();

if(obj_game.switches[? "quest_lp_started"] == true){
	active = true;	
}


//Collision
if(place_meeting(x,y,obj_player) && active){	
	obj_game.switches[? "quest_lp_got"] = true
	instance_destroy();
}