/// @description Insert description here
// You can write your code in this editor
if(global.ready == true){

	var current_spawn  = spawn_data[spawn];

	var len = array_length_1d(current_spawn) -1;

	switch(len){
	
		case 0: script_execute(current_spawn[0]); break;
		case 1: script_execute(current_spawn[0], current_spawn[1]); break;	
		case 2: script_execute(current_spawn[0], current_spawn[1], current_spawn[2]); break;
		case 3: script_execute(current_spawn[0], current_spawn[1], current_spawn[2],current_spawn[3]); break;
		case 4: script_execute(current_spawn[0], current_spawn[1], current_spawn[2],current_spawn[3], current_spawn[4]); break;
		case 5: script_execute(current_spawn[0], current_spawn[1], current_spawn[2],current_spawn[3], current_spawn[4], current_spawn[5]); break;
	
	}
}