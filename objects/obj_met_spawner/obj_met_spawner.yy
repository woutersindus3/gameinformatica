{
    "id": "62cb0a8b-e6bc-4d44-a3cb-667d8bbcd023",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_met_spawner",
    "eventList": [
        {
            "id": "df6c339e-4057-4102-9458-e7c61ee80a0e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "62cb0a8b-e6bc-4d44-a3cb-667d8bbcd023"
        },
        {
            "id": "f4d2fe32-3ad7-46dc-8254-f23324357134",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "62cb0a8b-e6bc-4d44-a3cb-667d8bbcd023"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "2628879e-7069-4e26-a5a3-6ac1a439c2e7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "1aa8f4b8-6821-44e1-aadc-9919cccf61ac",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "f5ca61a6-9823-4f7c-adfc-6e5a2157579f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "7a1d8842-ce1b-4c15-9821-dbcbb74899f4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}