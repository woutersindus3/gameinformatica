{
    "id": "d58f210a-226a-4a6f-b572-52a5ac993d07",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_quest_object_template",
    "eventList": [
        {
            "id": "9678c66f-c5cd-42db-aee7-835a71cc6092",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d58f210a-226a-4a6f-b572-52a5ac993d07"
        },
        {
            "id": "95f68321-73ab-4c0b-9aaa-9405bbeafbbb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d58f210a-226a-4a6f-b572-52a5ac993d07"
        },
        {
            "id": "84a6c39d-f3d5-4dce-a9ae-d2b723eee1d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d58f210a-226a-4a6f-b572-52a5ac993d07"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fedaa4c1-45fb-4eaa-b3ff-c510d8518d3f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "232837b5-86b6-4324-98c6-b0beea6d1e67",
    "visible": false
}