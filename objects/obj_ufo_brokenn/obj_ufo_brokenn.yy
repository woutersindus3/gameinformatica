{
    "id": "15c6f1a0-f2f7-41e2-9d50-6f2527445100",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ufo_brokenn",
    "eventList": [
        {
            "id": "f889f501-b8ef-49a4-b8ad-0e6ecc658259",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "15c6f1a0-f2f7-41e2-9d50-6f2527445100"
        },
        {
            "id": "a46d82bb-eadf-4be2-a22c-a5c3d0cc3d18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "15c6f1a0-f2f7-41e2-9d50-6f2527445100"
        },
        {
            "id": "7806cea5-65aa-4e9c-a62c-a22407930c58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "15c6f1a0-f2f7-41e2-9d50-6f2527445100"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fedaa4c1-45fb-4eaa-b3ff-c510d8518d3f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e1404217-a9ab-4eef-9163-eb0f740ab0ee",
    "visible": false
}