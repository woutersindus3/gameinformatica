event_inherited();

questtext = [
	["It's quite broken, is it? We need to get away from here as quick as possible. The headache is already unbearable.",
	"We are only missing 2 parts. The anti gravity crystal, and the anti gravity motor. Let's have a look on the planet to see if we can find some help."],
	["If we can get our hands on those parts we can get the fack out of here. They shouldn't be to hard to get our hands on",
	"I believe there is some wierd ravian guy who found some wierd ass crystal/gem kinda thing"],
	["We finally got all the parts, we only need to get away now. That shouldn't be to difficult to do!"]
];

hasquest = true;
quest = "quest_ufo_started";
questcondition = "quest_ufo_got";
questfinished = "quest_ufo_finished";

name = "You";
voice = snd_you;
portrait =  spr_astonaut_talk;