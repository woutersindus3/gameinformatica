event_inherited();

questtext = [

	["I am so thirsty, i can use a beer real bad. I see you have a problem with you motor, I have got a spare one i found the other day. If you get me a beer I can give it to you."],
	["Rember that motor dude!"],
	["Thanks, was about time. Was about to die to dehydration."]
	
]

name = "Eppo"
portrait = spr_raveian_hobo_dialog;
voice = snd_larry;


hasquest = true;

quest = "quest_beer_started";
questcondition = "quest_beer_got";	 
questfinished = "quest_beer_finished";
