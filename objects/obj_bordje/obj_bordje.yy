{
    "id": "efef9187-463e-46b4-8f42-e0b0ca22b4ae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bordje",
    "eventList": [
        {
            "id": "ca108653-e3a0-4a79-98d9-f14350f12219",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "efef9187-463e-46b4-8f42-e0b0ca22b4ae"
        },
        {
            "id": "0b9cb8c9-9cb5-4452-9e65-5777310de362",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "efef9187-463e-46b4-8f42-e0b0ca22b4ae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fedaa4c1-45fb-4eaa-b3ff-c510d8518d3f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5aedc3d1-8ab6-407a-89f2-e140dc053136",
    "visible": false
}