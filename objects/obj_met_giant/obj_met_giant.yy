{
    "id": "61fe50f2-8e43-4de3-b2ef-b95afd7057e1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_met_giant",
    "eventList": [
        {
            "id": "f370cdfc-0ed8-4df8-895c-9bf5554a6e75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "61fe50f2-8e43-4de3-b2ef-b95afd7057e1"
        },
        {
            "id": "b6a90efd-ac82-41fd-ada1-b9ab51be6f17",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "61fe50f2-8e43-4de3-b2ef-b95afd7057e1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "8582fdbd-f0aa-40f5-8734-607e68d639f8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "e61ee046-9943-4cf1-8e38-29a96c0841a9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "98c4c2f2-373f-41fd-8090-47b68522dbe5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "1873e74d-3b0b-46d0-83ad-45d423a6d284",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d7bae69b-f002-4baa-8d95-48927d387278",
    "visible": true
}