{
    "id": "3a1f2b2a-c54f-4746-afb8-0f7be7600f35",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ufo_temp",
    "eventList": [
        {
            "id": "ab70b950-f519-4fb2-9ddf-0809d632c9e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3a1f2b2a-c54f-4746-afb8-0f7be7600f35"
        },
        {
            "id": "52a4038c-e254-49c9-b177-997bfddedcc0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3a1f2b2a-c54f-4746-afb8-0f7be7600f35"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6d68adfd-9ba6-4548-bdcc-4891b985027d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "9966edae-8e8b-4538-9fda-d2b8b0f9246f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "09143475-349e-4d29-875f-1d64078fc6d1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "3d289689-1df7-4bee-9ff6-752446502244",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 16
        },
        {
            "id": "d94d36c7-14c0-429c-bb36-f8c56670cdf0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "440f00aa-4234-41c3-9878-7e1fbc71dbda",
    "visible": false
}