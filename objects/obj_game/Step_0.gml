//test if main mission is completed

if(switches[? "quest_beer_finished"]) obj_inventory.motor = true;

if(switches[? "quest_beer_finished"] and switches[? "quest_lp_finished"]	){
		
		
		
		switches[? "quest_ufo_got"] = true;
		
}

if(room != rm_wake && room != rm_end){

	if(obj_game.switches[? "quest_ufo_finished"] == true){
		endtimer++
	
		if(endtimer > 60){
			endtimer = 0;
			
			instance_deactivate_object(obj_depthsorter);
			//instance_deactivate_object(obj_game);
		
			scr_slide_transition(TRANS_MODE.GOTO,rm_wake);
			
			//--lp
			switches[? "quest_lp_started"]  = false;
			switches[? "quest_lp_got"]		= false;
			switches[? "quest_lp_finished"]	= false;


			//--ufo
			switches[? "quest_ufo_started"] = false;
			switches[? "quest_ufo_got"]		= false;
			switches[? "quest_ufo_finished"]= false;

			//--Beer
			switches[? "quest_beer_started"] = false;
			switches[? "quest_beer_got"]	 = false;
			switches[? "quest_beer_finished"]= false;

			//--coin
			switches[? "quest_coin_started"] = false;
			switches[? "quest_coin_got"]	 = false;
			switches[? "quest_coin_finished"]= false;
			}
	
	}
}