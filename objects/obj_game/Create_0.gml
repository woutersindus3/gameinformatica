endtimer = 0;

//Transition
global.transition_done = false;
spawnX = -120;
spawnY = -120;

//Dialoog



//quest
switches = ds_map_create();

//Kopier wat er bij lp staat en verander lp naar de nieuwe naam van de quest zoals dit

/*
switches[? "quest_[eigenaam]_started"]  = false;
switches[? "quest_[eigenaam]_got"]		= false;
*/ 

//--lp
switches[? "quest_lp_started"]  = false;
switches[? "quest_lp_got"]		= false;
switches[? "quest_lp_finished"]	= false;


//--ufo
switches[? "quest_ufo_started"] = false;
switches[? "quest_ufo_got"]		= false;
switches[? "quest_ufo_finished"]= false;

//--Beer
switches[? "quest_beer_started"] = false;
switches[? "quest_beer_got"]	 = false;
switches[? "quest_beer_finished"]= false;

//--coin
switches[? "quest_coin_started"] = false;
switches[? "quest_coin_got"]	 = false;
switches[? "quest_coin_finished"]= false;
