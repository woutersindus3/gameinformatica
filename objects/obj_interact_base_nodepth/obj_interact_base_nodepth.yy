{
    "id": "af372286-8008-4f92-bb86-d3369ce6179c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interact_base_nodepth",
    "eventList": [
        {
            "id": "bf15ebbc-0735-4c7f-ba40-609769c52e72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "af372286-8008-4f92-bb86-d3369ce6179c"
        },
        {
            "id": "887e52a4-742b-4156-893f-19a2fe4334e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "af372286-8008-4f92-bb86-d3369ce6179c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6d68adfd-9ba6-4548-bdcc-4891b985027d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}