box = spr_dialog_box;
frame = spr_portait;
portrait = spr_astonaut_talk;
namebox = spr_namebox;

voice = snd_you;

box_width = sprite_get_width(box);
box_height = sprite_get_height(box);
port_width = sprite_get_width(frame);
port_height = sprite_get_height(frame);
namebox_width = sprite_get_width(namebox);
namebox_height = sprite_get_height(namebox);

port_x = (display_get_gui_width() - box_width - port_width)*.5;
port_y = (display_get_gui_height()*.98) - port_height;
box_x = port_x + port_width;
box_y = port_y;
namebox_x = port_x;
namebox_y = box_y - namebox_height;

x_buffer = 16;
y_buffer = 16;
text_x = box_x + x_buffer;
text_y = box_y + y_buffer;
name_text_x = namebox_x + (namebox_width/2); 
name_text_y = (namebox_y + (namebox_height/2)); 

text_max_width = box_width - (2*x_buffer);

counter = 0;
pause = false;
str_len = 0;

text = -1;
name = "You"
page = 0;

interact = ord("E");
once = false;

text_col = c_white;
name_text_col = c_white;
font = fDialog;

draw_set_font(font);
text_height = string_height("M") ;

