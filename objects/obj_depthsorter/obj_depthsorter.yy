{
    "id": "72df399e-bc83-465d-ab1e-ab3a1d8ff0b0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_depthsorter",
    "eventList": [
        {
            "id": "f6fc668e-de53-4218-8b4c-22ca4bea0dde",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "72df399e-bc83-465d-ab1e-ab3a1d8ff0b0"
        },
        {
            "id": "3286e9b5-5578-4de2-82c5-5d9740516cc1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "72df399e-bc83-465d-ab1e-ab3a1d8ff0b0"
        },
        {
            "id": "62f14f57-0e43-437b-bb79-15630fa71f65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "72df399e-bc83-465d-ab1e-ab3a1d8ff0b0"
        },
        {
            "id": "1fcfb2b3-63a9-4115-a1a0-19864380a9b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "72df399e-bc83-465d-ab1e-ab3a1d8ff0b0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}