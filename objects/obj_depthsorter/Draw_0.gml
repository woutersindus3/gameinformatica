var inst_num = instance_number(par_depth);
var dgrid = ds_depthgrid;

ds_grid_resize(dgrid, 2, inst_num);

//Add instances in de lijst

var yy = 0; with(par_depth){
	
	dgrid[# 0, yy] = id;
	dgrid[# 1, yy] = y;
	yy++;
}

ds_grid_sort(dgrid,1,true);

//Loop door grid en teken alle instasties

var inst;yy = 0;repeat(inst_num){
	
	//pull id
	inst = dgrid[# 0 , yy];
	
	with(inst){
		event_perform(ev_draw, 0);	
	}
	yy++;
}