///@discription
///@arg object
///@arg currentx
///@arg currenty
///@arg xto
///@arg yto
///@arg time

inst = argument0;
xto = argument3;
yto = argument4;
time  = argument5;
currentx = argument1;
currenty = argument2;


if(inst.x == currentx || inst.y == currenty){
	hspd = (xto - inst.x)/(time*room_speed);
	vspd =  (yto - inst.y)/(time*room_speed);
}

show_debug_message(inst.y);
show_debug_message(yto);

if(!(inst.x < xto && inst.y > yto)){
	inst.phy_position_x += hspd;
	inst.phy_position_y += vspd;
}else{

	scr_end_action();
	
}

