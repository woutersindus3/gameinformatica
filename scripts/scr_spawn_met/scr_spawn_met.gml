///@discription scr_spawn_met
///@arg xpos
///@arg ypos
///@arg spd
///@arg metc
index = argument3;

var inst = instance_create_depth(argument0,argument1,-1000,obj_met);
inst.spd = argument2;


switch(index){
	case 1:
		inst.sprite_index = spr_met_01;
		break;
	case 2: 
		inst.sprite_index = spr_met_02;
		break;
	default:
		inst.sprite_index = spr_met_01;
		break;
}

scr_end_spawn();