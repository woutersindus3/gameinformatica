///@desc scr_wrap(string, max_width)
///@arg string
///@arg max_width

var str = argument0
var max_width = argument1;

var str_length = string_length(str);
var last_space = 1;

var substr;

var count = 1;repeat(str_length){
	substr = string_copy(str,1,count);
	if(string_char_at(str,count) == " "){
		last_space = count;	
	}
	
	if(string_width(substr) > max_width){
		string_delete(str,last_space,1);
		str = string_insert("\n",str,last_space);
	}
	
	count++;
}

return str;