///@description scr_change_background
///@arg bgname
///@arg spritename

var lay_id = layer_get_id(argument0);
var back_id = layer_background_get_id(lay_id);

//argument1.image_index = 1;
//argument1.image_speed = 0;

layer_background_sprite(back_id, argument1);

scr_end_action();