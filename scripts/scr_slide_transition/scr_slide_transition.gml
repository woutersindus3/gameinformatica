/// @desc scr_slide_transition(mode, targetroom)
/// @arg Mode 
/// @arg Target

if(!global.transition_done){
	global.transition_done = true;	
}

with(obj_transition)
{	
	mode = argument[0];
	if (argument_count > 1) target = argument[1];
}