{
    "id": "4f6b982d-b966-4404-8c47-36577257399a",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fCut",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "8BIT WONDER",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4932fd9e-9af9-4dcd-8098-82c5975a8b88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 185,
                "y": 78
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "482fe054-e26c-4abd-92d4-13fd3dc74e0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 173,
                "y": 97
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "4a5c0519-a8c9-4c71-a950-ad4c0534d4ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 136,
                "y": 97
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b248b298-5531-4ec5-9248-6109e65c6f7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f21a1888-938c-498f-a301-cbfedb30f213",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 97
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2f732544-003b-40f3-b547-c4db0721300a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 148,
                "y": 78
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "cb2e1c72-e429-40d1-ba02-da7ac9c7ed56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 135,
                "y": 78
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b5f02339-8dcd-4085-8a11-fbb02f70a6d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 17,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 168,
                "y": 97
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "a8947e43-8b58-41cc-a1dd-5481175581ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 218,
                "y": 78
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b18ffc89-6914-4386-b0ed-352bb7948e81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 229,
                "y": 78
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b247ca70-7ad2-48dc-a94d-67fce930704f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "bb73c955-6d58-4275-baa3-d4055f47d7cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 12,
                "y": 97
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "935b2f76-23f0-4801-80b7-912cd8788d74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 144,
                "y": 97
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "117108ee-f15c-40f4-ba6d-b2abb239bc49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 112,
                "y": 97
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "48cb08b1-45c7-41af-93c6-5426617a7149",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 156,
                "y": 97
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f08d4382-e2e8-4683-b588-3397ef724487",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 97
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "4e63b6c5-87f6-4dbb-aeb1-7ad462e75efa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 104,
                "y": 59
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "81607d8c-3020-4225-a12e-c1d65d778e69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": -2,
                "shift": 9,
                "w": 8,
                "x": 22,
                "y": 97
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d4fd7815-f337-4718-baf4-1758b404ccf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 138,
                "y": 59
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "372e092c-fea0-4c4b-9b42-bd12d9557bf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 87,
                "y": 78
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ff07448e-d70b-4ce1-9f17-dbb667028019",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 70,
                "y": 78
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "bdf0dbe1-a059-418c-a078-da2650cdf4b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 53,
                "y": 78
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "587f42e7-30ec-466e-b171-501ce4de478a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 36,
                "y": 78
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "28639dc3-fec3-48cd-878a-0aae831731b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 19,
                "y": 78
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f10eb11f-2c70-4d8a-9ea1-057355e54a83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 104,
                "y": 78
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "de29cfb0-3b4b-4fcd-9f04-e5574e6dc7ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 223,
                "y": 59
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "47aaaf65-74f1-42ec-9431-9aacab04c9cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 178,
                "y": 97
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b26881a2-40b5-46bd-82e0-3f6d121480fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 162,
                "y": 97
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4872b478-e216-4a96-9a60-561d2fd3a340",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 32,
                "y": 97
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "48da9233-5829-4ce5-8def-8a4b48927c1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 82,
                "y": 97
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ae6e0811-c4b9-4f2a-87d5-ec7acafa96e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 102,
                "y": 97
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b61f3f99-172e-4588-affe-760f78133789",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 52,
                "y": 97
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "aebae85d-2fa6-4ac9-a8ee-c0cd690029bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 173,
                "y": 78
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "58031382-6f4b-4612-9e4d-a3f29285259d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 104,
                "y": 40
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e763b0e1-0cb7-43f4-ab35-f4d1ff086e2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 104,
                "y": 21
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "1d96d6cb-b8da-4308-90ad-286a13f779c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 36,
                "y": 21
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e5798845-793e-4d39-80cc-86f14a2631df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 19,
                "y": 21
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "813e6aa8-76ea-4f84-81a9-9c0c66d2d8bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 21
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e266568f-0de8-4b54-b722-2aa493478547",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9234b89c-8d47-4fcb-bd11-ac22a3a8b5d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6b6e7c6e-fc3f-46e5-b0a7-6c2771e7c42a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 53,
                "y": 21
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f0d43810-da47-44d8-8204-5c209b3940cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 6,
                "x": 128,
                "y": 97
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ed0f2acf-479f-4f9a-b28e-daa8a7cc1ab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "12634cb3-0f4a-46a6-aa52-f9551557b9d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "3b68f6ef-ded8-4d51-9c7b-0a8188fc849f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "dd75de16-979e-4bf1-b54e-43af71c83171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 0,
                "shift": 26,
                "w": 24,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "6a4670a1-00fb-4a09-8dba-be246289880d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 70,
                "y": 21
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "48db4eed-c83d-47bd-94e3-26b153fbc084",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "965610d0-e540-4cf6-8a4f-da91602a5c65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 87,
                "y": 21
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a1fdd354-4892-45c0-b958-346c282b281e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 121,
                "y": 21
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "973ac9fa-9ecd-4ce1-8fe1-6ea050c10baa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 138,
                "y": 21
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e46902d0-59fc-4d9d-8462-d87f7d2d3dd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 155,
                "y": 21
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "6d3d7828-710b-4302-9393-eea9c9181111",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 172,
                "y": 21
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b6f30c6c-fff7-4e2a-994e-8e74cb2f246d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 189,
                "y": 21
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "297baa9a-d32b-44f9-a4b1-8bd44416f782",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 206,
                "y": 21
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a4469fa2-d561-4652-a0d8-197ea05c0bb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 0,
                "shift": 26,
                "w": 24,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "3e7928d1-c12b-4d48-ab16-b52596282954",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 223,
                "y": 21
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b889c8a8-b478-4264-b881-6cd6b02ddf72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "54e4a269-19f9-41e8-a683-4542dd9a1d68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 19,
                "y": 40
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ef8ef763-0fbc-485f-ade7-f6350324d713",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 207,
                "y": 78
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1e856ee5-36fd-41b9-9e2e-c4af23b0418e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 62,
                "y": 97
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "6724ef6b-2cc8-4849-be49-356760a16ff2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 196,
                "y": 78
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "1ba65bf5-c7de-475e-8bda-ef63fdb8161a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 161,
                "y": 78
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "99848076-35e4-4cfe-8713-6cf43d812961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 17,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 121,
                "y": 78
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7da13150-1760-4379-a3e7-4b683b269750",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 150,
                "y": 97
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "1e6a8840-53fe-44a4-9ecd-7a3f6ba77f06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 121,
                "y": 40
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "96081121-bdc8-4f16-b9c0-520b2c06ce3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 87,
                "y": 59
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ef266b60-416d-43ff-9280-0f649ecc3ee7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 70,
                "y": 40
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "dcda9222-2905-4a43-90bd-3732961e0a61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 53,
                "y": 40
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "3138a315-5f42-467a-a839-614981df6fee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 36,
                "y": 40
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "4b55c2d7-1f75-4050-adc0-5a6ee951c8a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 87,
                "y": 40
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3f19dac8-1227-4599-9fd0-b4ac0c388aa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "91fbb5c3-0c92-4d87-bee4-89203d1e6a35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 138,
                "y": 40
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c51af57b-f20e-42cd-8859-35b60709a624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 6,
                "x": 120,
                "y": 97
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "873316ff-d6f1-4d38-acf4-a6f4d659a4f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 155,
                "y": 59
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "7a58a895-03be-4b84-829c-940053dee3a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 172,
                "y": 59
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "6003ef10-6972-48eb-8207-a3de0101f534",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 189,
                "y": 59
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4b8d6e0e-3067-4f78-b70e-dfefc27ba730",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 0,
                "shift": 26,
                "w": 24,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "6bab59c3-2cbc-4a96-9417-14aa434160bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c299b37b-cd6b-4ed2-aeb0-043c9c4895aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 206,
                "y": 59
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "938d1a20-d08b-427d-85b9-3657f4b324ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 121,
                "y": 59
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "481c9e55-0a17-4135-9500-abea0b5651b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 70,
                "y": 59
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ad1ebaf8-d258-4d06-8117-4ba44e5f124a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 53,
                "y": 59
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "2358c3aa-3892-4043-b3f3-9466134f8752",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 36,
                "y": 59
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "76dd83e8-335c-4f28-90d4-27503039a39a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 19,
                "y": 59
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "256d5564-2ebf-4add-bf77-6f8ad52183c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 223,
                "y": 40
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c43445f7-c207-4b0e-b4f0-e369086ee5be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 206,
                "y": 40
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "92188d28-06dc-413f-8a92-a117d676a9d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 0,
                "shift": 26,
                "w": 24,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2f141cbe-c590-4bb3-a43a-a6481abd48bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 189,
                "y": 40
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "64979ed2-3c8f-437f-a6d9-94dafee9490c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 172,
                "y": 40
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b402144c-3c6e-4a24-af6f-09033080264b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 155,
                "y": 40
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "f947aacd-73cc-48a5-9083-8d399ed33d4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 240,
                "y": 78
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "0091803e-177c-4437-993e-b3526926ab33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 17,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 183,
                "y": 97
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e1ef3fd2-dbd4-473c-b360-3d64a9987bde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 72,
                "y": 97
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "45cc0152-6468-40c2-a0c9-162599759267",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 92,
                "y": 97
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 13,
    "styleName": "Nominal",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}