{
    "id": "fa87ddc1-a311-4a79-8e0b-febf942e7bda",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fWake",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "8BIT WONDER",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "04e575a8-874b-4b45-8aaa-d0da3fa1ba0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 77,
                "offset": 0,
                "shift": 39,
                "w": 39,
                "x": 463,
                "y": 318
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "bc1160ad-217a-4c13-93fc-e21daca33f98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 77,
                "offset": 18,
                "shift": 46,
                "w": 10,
                "x": 334,
                "y": 397
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "18c69986-5257-4753-b224-df2babb8b804",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 77,
                "offset": 10,
                "shift": 46,
                "w": 26,
                "x": 134,
                "y": 397
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b6b74824-8ad7-43b8-97a2-e834d3f86949",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 78,
                "x": 426,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "6178e72a-e5cd-437d-be2d-55e46e52c052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 77,
                "offset": 6,
                "shift": 46,
                "w": 34,
                "x": 935,
                "y": 318
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "cef08491-059e-40db-88cd-cd72dae6a7c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 77,
                "offset": 0,
                "shift": 46,
                "w": 47,
                "x": 320,
                "y": 318
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5dd55c37-8aad-4664-9a8d-6ae4805945b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 77,
                "offset": 2,
                "shift": 46,
                "w": 45,
                "x": 369,
                "y": 318
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e1ecf99d-f738-4716-affc-cb3cfffc6781",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 77,
                "offset": 18,
                "shift": 46,
                "w": 10,
                "x": 310,
                "y": 397
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c9b36146-5607-4cf6-8b73-37bd5e160a74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 77,
                "offset": 0,
                "shift": 39,
                "w": 39,
                "x": 586,
                "y": 318
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "69a74e35-93ac-4fdd-8852-1828f3f63fb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 77,
                "offset": 0,
                "shift": 39,
                "w": 39,
                "x": 668,
                "y": 318
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "52fdb240-2bc1-4e7e-afcf-c36f20d6b15d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 739,
                "y": 160
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a38da2f9-d1f7-4007-89c9-600058dfb210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 77,
                "offset": 5,
                "shift": 46,
                "w": 36,
                "x": 709,
                "y": 318
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "3c4e351d-3797-44b9-bff3-5291916f10cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 77,
                "offset": 16,
                "shift": 46,
                "w": 14,
                "x": 280,
                "y": 397
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4ae49c25-67ea-46c9-b8ee-6e3437f1a2ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 77,
                "offset": 10,
                "shift": 46,
                "w": 26,
                "x": 190,
                "y": 397
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "605eb9a6-dc03-400a-a62f-e196890ac76d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 77,
                "offset": 17,
                "shift": 46,
                "w": 12,
                "x": 296,
                "y": 397
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5d7728b0-218a-43ef-8064-75113c9623ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 77,
                "offset": 7,
                "shift": 46,
                "w": 32,
                "x": 2,
                "y": 397
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "6d0074d1-fa00-4307-bf6d-29f23cf1675d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 471,
                "y": 160
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d2647647-9923-4522-88b9-62e447828100",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 77,
                "offset": -7,
                "shift": 39,
                "w": 33,
                "x": 971,
                "y": 318
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "73b15ccd-9a99-4ef3-a7d6-a5023c4c40bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 337,
                "y": 239
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "40a63b27-91ff-4cfb-be01-c70fd2016e4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 136,
                "y": 318
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "230bf256-5c33-4e91-b9fe-be80215e5c9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 404,
                "y": 239
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ab6fdc81-73d1-4130-aa4a-14f760ae7a71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 471,
                "y": 239
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "eab8af74-ab3a-43f5-b1a8-8d4f53098fc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 538,
                "y": 239
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f74b92c1-23b8-4000-a6b5-4352fdb01921",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 605,
                "y": 239
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f141fb2b-d0f6-4f70-8040-2b1f1259ed99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 672,
                "y": 239
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d3b43064-4558-4d73-9326-63866912f318",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 739,
                "y": 239
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "14b4d461-4d2a-468a-b176-cead4448f4d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 77,
                "offset": 18,
                "shift": 46,
                "w": 10,
                "x": 322,
                "y": 397
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d96178f8-2aae-447c-811d-cb0879d79380",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 77,
                "offset": 15,
                "shift": 46,
                "w": 14,
                "x": 264,
                "y": 397
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "0aecf386-933b-4943-8b16-01bbe6cbd007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 77,
                "offset": 5,
                "shift": 46,
                "w": 36,
                "x": 747,
                "y": 318
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a5fe6f83-56d4-49c7-ac92-cf62384bb752",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 77,
                "offset": 5,
                "shift": 46,
                "w": 36,
                "x": 785,
                "y": 318
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "7606d56b-4c7d-40b5-ad47-7a3250cc55db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 77,
                "offset": 5,
                "shift": 46,
                "w": 36,
                "x": 861,
                "y": 318
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "cf8f96cb-dd85-4b92-a2ac-404cd3317fbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 77,
                "offset": 6,
                "shift": 46,
                "w": 34,
                "x": 899,
                "y": 318
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "bac5c16a-7694-4a88-a536-86763a765290",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 77,
                "offset": 1,
                "shift": 46,
                "w": 45,
                "x": 416,
                "y": 318
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "118330ef-08e6-496f-a294-a21e4b0e1a16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 136,
                "y": 239
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "de558125-e9ff-4459-8334-d1ea002f691c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 337,
                "y": 160
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "5c7af9a7-54e8-4f6c-97b7-889c1e9aae18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 404,
                "y": 81
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "6083543d-e572-4990-b905-99a120c57b35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 136,
                "y": 81
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "70683454-7f19-4de2-adb5-dad0473ad176",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 69,
                "y": 81
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "147cedd0-3ab3-4e17-9832-42a517fa806d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 2,
                "y": 81
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "6a132766-7c97-4832-abad-41a83910b887",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 908,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "0ab4f9bd-cfce-4dda-920a-cf0e69de8ff3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 774,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "117270a4-14e7-4d85-b94e-b093cb64a528",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 77,
                "offset": 0,
                "shift": 39,
                "w": 26,
                "x": 162,
                "y": 397
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c91cb41b-92bd-4b89-9b2f-fa3cbab69310",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 707,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "052ca694-5fe2-45c8-b85e-fbbc35d5187b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 640,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "66ca7c44-3959-4ac0-ae56-63b16e65de60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 573,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "35d8681a-5938-4e47-8f66-3474f35a69d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 77,
                "offset": 0,
                "shift": 116,
                "w": 104,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "dc0a2ab1-31c2-48f0-8cfb-e3d0c8cd8747",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 841,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "64b77197-1d04-4714-be2f-ed305ab5dbf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 506,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "995dad71-df90-4015-8253-ec465f4a0249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 270,
                "y": 160
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "78327044-9722-4c42-a560-9eb04850a0b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 337,
                "y": 81
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "42241dda-8b65-4ef4-82aa-501e75c969b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 471,
                "y": 81
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3d4aa95e-ad38-4058-b441-a5ff72ad0f21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 538,
                "y": 81
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "6b2d05f9-424e-4fd9-b49b-bee4f571f0bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 605,
                "y": 81
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "eee3cac9-75ec-47d3-a86a-ee9fc191217d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 672,
                "y": 81
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8f314f8c-79c9-4587-8b11-5b06a84a84a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 739,
                "y": 81
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "05dcbf1c-bb50-40d9-ad23-a65aef0910b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 77,
                "offset": 0,
                "shift": 116,
                "w": 104,
                "x": 320,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "92d3c79c-c959-4c72-8f31-ec1a28b41ade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 806,
                "y": 81
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "a96f4bcd-4c93-4ed3-a4de-56b7b2b4b219",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 873,
                "y": 81
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7fe17779-2ce7-4eca-a9fe-1860363d8f07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 940,
                "y": 81
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c8b2b1bb-a2de-4d3c-a076-b2c680204cf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 77,
                "offset": 0,
                "shift": 39,
                "w": 39,
                "x": 627,
                "y": 318
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "4dcc2862-4cc5-49fb-8839-eb45c96f1347",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 77,
                "offset": 7,
                "shift": 46,
                "w": 32,
                "x": 36,
                "y": 397
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "dd25074d-3351-4060-92e5-e8bc06a1262f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 77,
                "offset": 0,
                "shift": 39,
                "w": 39,
                "x": 545,
                "y": 318
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e6f16b02-0c9a-497f-9285-b400575dad09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 77,
                "offset": 4,
                "shift": 46,
                "w": 39,
                "x": 504,
                "y": 318
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "29a5c209-f733-4214-ac09-579370bef139",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 77,
                "offset": -1,
                "shift": 46,
                "w": 48,
                "x": 270,
                "y": 318
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7dc0ff38-6839-4273-9116-5bbb572f306b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 77,
                "offset": 15,
                "shift": 46,
                "w": 16,
                "x": 246,
                "y": 397
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "880545f2-25c1-43c6-8e7a-b815298ec532",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 404,
                "y": 160
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "bc8ba4fb-5175-48ca-abba-a4692e05943b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 203,
                "y": 160
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "311c68e7-6efb-47d9-8862-2ac7e21875e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 136,
                "y": 160
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "67dd84e1-dbcc-4da7-8c93-dc8943f62f8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 69,
                "y": 160
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "53cef813-c793-43ca-aefe-7f2cc7931f81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 2,
                "y": 160
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "0e419d22-53b9-4020-ad4e-be0a0b5c3d27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 270,
                "y": 81
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "78c1d552-fed1-4751-89c2-79b70582a34a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 203,
                "y": 81
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e6f4973d-9aa8-4c7c-b681-c19ad40b2fba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 69,
                "y": 318
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c97a0980-bdc4-4cda-b2fb-84f753841f80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 77,
                "offset": 0,
                "shift": 39,
                "w": 26,
                "x": 218,
                "y": 397
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "f77b8754-f13a-4f3e-8a64-05fd04083050",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 2,
                "y": 318
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "c14f60a5-7ab8-4794-a335-72ff26c418b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 940,
                "y": 239
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "fcc6d59c-1b44-4c9e-b7d7-afddbba2b474",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 873,
                "y": 239
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "1903c3a3-8676-4d46-9704-5da791ca1a91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 77,
                "offset": 0,
                "shift": 116,
                "w": 104,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c9539bcd-453c-45eb-9e2c-045a82d1095c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 806,
                "y": 239
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "663af9c2-2d23-4a5d-85cd-1994c5aef2e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 203,
                "y": 239
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b82d6a1a-971e-414a-8b0a-f7a04e0e020d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 69,
                "y": 239
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "78979c22-6d31-481b-91ca-74bf84d7b6d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 2,
                "y": 239
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "1c731a10-c0c6-4a45-8925-b103b3edeac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 940,
                "y": 160
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "7f3e4e76-9704-4d49-a0c3-669f65de66df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 873,
                "y": 160
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "455d059a-1b26-41f7-9d36-8e9b3f6ae746",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 806,
                "y": 160
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "48292519-4a73-4ec3-91d6-055f47346e30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 672,
                "y": 160
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "00713b6d-d031-4ff0-9869-04094c66a508",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 605,
                "y": 160
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "5fc3735f-6cb7-457d-8ac3-5c01356bc3c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 77,
                "offset": 0,
                "shift": 116,
                "w": 104,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f0e07cf8-16cc-40cf-8a8d-b5f448f22512",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 203,
                "y": 318
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "699efbd0-2763-4d7a-8359-850d4516fd92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 270,
                "y": 239
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c47ca5a2-940d-4420-b3de-b3acdb56e5fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 77,
                "offset": 0,
                "shift": 77,
                "w": 65,
                "x": 538,
                "y": 160
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "cb809b1e-6793-4a3e-92c3-4c3911e2af33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 77,
                "offset": 8,
                "shift": 46,
                "w": 30,
                "x": 70,
                "y": 397
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4368c9a2-6ec4-47fc-8f90-16bcdd3d0c7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 77,
                "offset": 20,
                "shift": 46,
                "w": 6,
                "x": 346,
                "y": 397
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5b55fc78-9738-4f55-86bd-7a053a772d1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 77,
                "offset": 8,
                "shift": 46,
                "w": 30,
                "x": 102,
                "y": 397
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "ad3ca3fa-e47a-490c-b632-a42360af87fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 77,
                "offset": 5,
                "shift": 46,
                "w": 36,
                "x": 823,
                "y": 318
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 58,
    "styleName": "Nominal",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}