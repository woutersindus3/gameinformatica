{
    "id": "42d0d282-2205-4f4a-85c9-fed59ed1bd58",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fMenu",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "8BIT WONDER",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4cd42ca0-ef03-41eb-ad1e-b179b4428c7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 215,
                "y": 154
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "f6825c9d-39c3-469a-b154-5d9bfdff643b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 36,
                "offset": 8,
                "shift": 22,
                "w": 6,
                "x": 136,
                "y": 192
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "428a59cf-0352-43bf-aa3f-ec6b8b91d60c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 36,
                "offset": 5,
                "shift": 22,
                "w": 12,
                "x": 36,
                "y": 192
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7e404792-35f8-45f7-b8d6-e6db5e10b6de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 36,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "c27c26c8-5366-42a8-adfc-e47992a57fae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 36,
                "offset": 3,
                "shift": 22,
                "w": 16,
                "x": 429,
                "y": 154
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "b317bd0f-c403-458d-abae-20b55826a686",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 36,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 123,
                "y": 154
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5320f17b-ffa3-4dad-8626-ef5337eabbb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 36,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 171,
                "y": 154
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f18802a4-0f3a-4d06-88ac-e90cb18b5f67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 36,
                "offset": 8,
                "shift": 22,
                "w": 5,
                "x": 144,
                "y": 192
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "21a80a63-32b6-4fc6-a4eb-ad6fffee79ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 255,
                "y": 154
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "70ea7e8a-c731-45d0-b954-6af3331a807d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 295,
                "y": 154
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b76a1cb3-41e7-4ae0-865e-accae6e0e2ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 386,
                "y": 78
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f2249743-01bc-4c5f-8381-8444b6a89ccc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 36,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 315,
                "y": 154
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c23a330c-65a5-441e-b25d-77b9e628bdf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 36,
                "offset": 7,
                "shift": 22,
                "w": 7,
                "x": 111,
                "y": 192
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a71b1d2d-5f94-4a3b-ba3d-2afc09197f0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 36,
                "offset": 5,
                "shift": 22,
                "w": 12,
                "x": 64,
                "y": 192
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "dc4f84c8-d9a7-4b01-aa3b-7043078a7acd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 36,
                "offset": 8,
                "shift": 22,
                "w": 6,
                "x": 120,
                "y": 192
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "cad574a0-9035-4cee-b50b-ef99c014ad86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 36,
                "offset": 3,
                "shift": 22,
                "w": 15,
                "x": 447,
                "y": 154
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3468e5df-091a-4efd-8db1-f08a2428aa27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 98,
                "y": 116
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "3514c4a3-2c16-49f2-8312-90ef7f04380e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 36,
                "offset": -3,
                "shift": 18,
                "w": 15,
                "x": 481,
                "y": 154
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1bda9a61-a6be-4a24-9731-58840197f58f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 162,
                "y": 116
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "95aba3d8-a692-40cd-b87a-f45727cd8bc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 34,
                "y": 154
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "5257cfea-1386-4d57-8716-94bc9d839a7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 2,
                "y": 154
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "efce2eb8-795b-4026-a273-460e06a22675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 450,
                "y": 116
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "842597f6-c025-451f-9f32-3fbe985b3ffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 418,
                "y": 116
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "3d34a9ac-836b-439c-b09f-b049e74881d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 386,
                "y": 116
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "d7b661ce-360a-41fd-8ba1-05ea63c5a885",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 66,
                "y": 154
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "06f1ecd3-dc4a-456d-8e4d-b9125e017710",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 322,
                "y": 116
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "4c03b56f-dc50-493a-ba3e-079a6c116ee9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 36,
                "offset": 8,
                "shift": 22,
                "w": 6,
                "x": 128,
                "y": 192
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d8805be9-5fcb-4505-8a13-97ebdb56992c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 36,
                "offset": 7,
                "shift": 22,
                "w": 7,
                "x": 102,
                "y": 192
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "83e2bb6e-a03c-4a11-b9cd-a9f6ea987b76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 36,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 334,
                "y": 154
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9bd18024-470a-4558-9451-c646be831e70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 36,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 353,
                "y": 154
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "54531fed-4355-42b0-a68c-e8871231f82b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 36,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 410,
                "y": 154
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f3de20ac-ca04-4546-9bed-cbf1dc14129d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 36,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 391,
                "y": 154
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "eeb09540-347e-43ff-9c18-3525844c4e1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 36,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 147,
                "y": 154
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "7504ef13-b492-42bc-acb3-d24deb73a0cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 130,
                "y": 78
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8099e72d-9fcd-446a-8122-6f3bb92938be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 162,
                "y": 40
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "cb45d946-4822-489d-a6cd-450a389c40ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 34,
                "y": 40
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "def7ed2f-0566-4bd2-921b-8310d558aa3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a4ade88c-173f-4d29-96db-1ae7e15d05c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 464,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "2dc9f816-80d8-4aef-affd-81f75db618c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 432,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "6b9634f7-dc3d-475c-960f-70377ed13fb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 400,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1854fed0-fe5f-4c84-a516-f1af06f7dba5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 66,
                "y": 40
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "632b4216-66a0-47ca-9657-42a10f7be0ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 12,
                "x": 50,
                "y": 192
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "bfb52d92-1c75-4a0c-b0df-b564d878b40c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 336,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "982a93e7-739a-4434-af9c-94fc2e196978",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 304,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "09025ff9-279f-405c-a91c-58bdf9ec0afc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 272,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e64ac2fa-495d-4699-890c-067c0d821a94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 36,
                "offset": 0,
                "shift": 54,
                "w": 48,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "55a659db-2ee0-403f-9d1e-a3f4ce6f5f79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 98,
                "y": 40
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e7e46b18-1483-4a5f-a1fa-94fce43de7a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "1fe2505f-2e60-4e09-8b57-a3b175e4b086",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 130,
                "y": 40
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "677452cc-cf2d-4257-bf16-c34ff3a32a31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 194,
                "y": 40
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "bf024f48-6c5b-4f14-9db9-024a931c27b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 226,
                "y": 40
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "4123b2b5-9c6a-4feb-aa6e-27b0fc117994",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 258,
                "y": 40
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "dcecf6d8-b8a2-4146-bfb9-294970269bdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 290,
                "y": 40
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "3c1df96e-2cbb-483a-bc4a-5191e17b4ca2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 322,
                "y": 40
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "88d96faa-1503-47ec-833f-878461471d88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 354,
                "y": 40
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "5a27b23f-b8bb-42a1-aec3-7a7338ebbb36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 36,
                "offset": 0,
                "shift": 54,
                "w": 48,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "6b7e12cb-d825-4420-bf4d-390dc8d1855c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 386,
                "y": 40
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "49602aa7-6629-44e1-a3f8-ed5b86c9956e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 418,
                "y": 40
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c3ed3a45-d10f-4509-a3fb-8b37ad3dd44c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 450,
                "y": 40
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "0909a70f-f9fe-47f4-b670-7057843a0d33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 275,
                "y": 154
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "969f5558-920a-4acd-b477-a1263d603905",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 36,
                "offset": 3,
                "shift": 22,
                "w": 15,
                "x": 2,
                "y": 192
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "7a809c9d-f3b7-42ed-b117-b121d4420850",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 235,
                "y": 154
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "861d965a-07ec-4995-99e4-e619034df81f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 36,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 194,
                "y": 154
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "541056de-f876-448c-b5d6-76b58280d707",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 36,
                "offset": -1,
                "shift": 22,
                "w": 23,
                "x": 98,
                "y": 154
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "cbad0adb-e4cb-4580-8efe-0ff5bfce5a5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 36,
                "offset": 7,
                "shift": 22,
                "w": 8,
                "x": 92,
                "y": 192
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f129117c-9ade-4a27-935e-8f2678962df4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 162,
                "y": 78
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b277617c-9917-499b-bbeb-d4948965a26c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 66,
                "y": 116
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "3c3e37d5-760a-44a5-b781-f2ad28fae58d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 66,
                "y": 78
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "34f76409-d46c-4fbf-8bc2-521a9a891364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 34,
                "y": 78
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "41448b49-d7b6-4925-b12e-03521452ad74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "93c5e45e-5df5-46f4-bb87-8661c78d510f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 98,
                "y": 78
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "edebc975-b6e5-4d19-a0d3-f11f5a735fa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 368,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a5bd4789-6a48-419e-ba29-cc72f9227aa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 194,
                "y": 78
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c05fc841-2588-41ac-935f-2328ea2e5d68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 12,
                "x": 78,
                "y": 192
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "aaae8084-e52d-404b-b0d2-113a28d86c44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 194,
                "y": 116
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b56854cd-77a9-4945-87a7-61cb4641cae4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 226,
                "y": 116
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e1df4cdb-21fd-4887-935d-4dcd8440d386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 258,
                "y": 116
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "143d9792-bbae-4e50-8ac2-29d3eec0f264",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 36,
                "offset": 0,
                "shift": 54,
                "w": 48,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b3eaa6a2-c154-4d7d-9fdf-b9ec6284f374",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 354,
                "y": 116
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "db77732a-e7b2-4050-a9c0-75d3f91ba342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 290,
                "y": 116
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9fcec6ac-2a44-429c-a86c-c4ddced06f89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 130,
                "y": 116
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "11761fae-8dcc-421d-8cf3-ae9d10220e65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 34,
                "y": 116
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "15121b6b-f09a-4089-91af-680e560df138",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 2,
                "y": 116
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "63332a8c-8190-4c06-8f32-8cd9983edbc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 450,
                "y": 78
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3ea5670a-38c1-4029-b33c-c684d2ff5e3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 418,
                "y": 78
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9ac6ed1e-4474-4c69-9700-ee6624595a52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 354,
                "y": 78
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "24c11ddb-9039-4476-835f-f010ecadcfc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 322,
                "y": 78
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e06e9dbb-f92f-4c01-9260-9bb657a5e7f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 36,
                "offset": 0,
                "shift": 54,
                "w": 48,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c88e39ed-ce72-4bee-947d-c544a373a159",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 290,
                "y": 78
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "aac11f0a-78de-4a3e-9d79-1c389f7b1262",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 258,
                "y": 78
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "6c620471-0925-4372-b1fb-f3e37c83610a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 36,
                "offset": 0,
                "shift": 36,
                "w": 30,
                "x": 226,
                "y": 78
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b15e74c5-e639-496d-9ef7-1efff672a501",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 36,
                "offset": 3,
                "shift": 22,
                "w": 15,
                "x": 464,
                "y": 154
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "cede62d8-689b-46b3-969b-a44df7a8548d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 36,
                "offset": 9,
                "shift": 22,
                "w": 4,
                "x": 151,
                "y": 192
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "520c8eda-b7f3-4331-8bc0-3d4167f890ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 36,
                "offset": 3,
                "shift": 22,
                "w": 15,
                "x": 19,
                "y": 192
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "eae34882-2425-4b4e-a997-c19b3c63e987",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 36,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 372,
                "y": 154
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 27,
    "styleName": "Nominal",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}