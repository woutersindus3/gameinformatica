{
    "id": "1d7efc0d-569a-4a2c-a8ea-4e8ffbe7c694",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fDialog",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "1b9c384c-9249-4b1f-b6da-c688e1bf93b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 143,
                "y": 68
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ae471057-166a-40a5-96be-ecec4f7c2305",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 207,
                "y": 68
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "35b2b40c-1994-4d85-9c7e-c017037259b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 81,
                "y": 68
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7dfe3222-99bb-4677-93cb-507bf7b48a2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 163,
                "y": 24
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "2e6bbcd3-7083-41a9-834d-3ab8cbea8ec9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 186,
                "y": 24
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a2e87666-6cc6-42e9-9c33-e9eb39a8b02d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 20,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e495fa8d-22da-497d-ab99-c7059221c22a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 54,
                "y": 24
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b2a61897-0ebb-4431-bdc9-13fa9fc83bf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 20,
                "offset": 1,
                "shift": 3,
                "w": 3,
                "x": 227,
                "y": 68
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ebd419e1-9c40-4d65-855f-701a400c400b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 171,
                "y": 68
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "cf30b780-33b7-4d8b-addd-ade298987fcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 157,
                "y": 68
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "6cd117da-06b5-4256-a36c-2fb79caaded7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 72,
                "y": 68
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "03256b62-d5ef-49c4-89e6-58e7f99034d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 219,
                "y": 24
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "29121e7b-0b7b-4252-84ef-dd13cee33bb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 232,
                "y": 68
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "7190af86-905a-4457-b78a-0814cd25d28f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 89,
                "y": 68
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "4d8f1f2b-e64b-4409-a7a6-50181aebeaaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 20,
                "offset": 2,
                "shift": 5,
                "w": 3,
                "x": 202,
                "y": 68
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d2d37363-c5d8-4c2f-8e0b-f17078905723",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 150,
                "y": 68
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "da46033f-4388-44f3-839e-1debe914e70d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 46
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5b6ceb7a-fc4e-48b8-8da2-1bb6f6fc025d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 20,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 105,
                "y": 68
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "368eb41a-8781-41bc-87d2-1222ae5aab9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 46
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "12c3325f-5712-4786-b90b-d91c667d133e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 167,
                "y": 46
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "654d77ad-33c8-4098-ad13-4f747ff6c70f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 46
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "31ea4c05-faf6-4a21-8beb-734e1d7bf938",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 46
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "80b04746-6282-4bfb-a807-dde309f6e1be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 134,
                "y": 46
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "60e7f355-3d1f-4005-840b-f2908df10904",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 145,
                "y": 46
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "799a2223-bb4c-42d7-bf91-383193a0b8fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 156,
                "y": 46
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "430e5bdc-067b-478a-b53c-54de31879cc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 178,
                "y": 46
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "aea407b0-46f1-4c2f-b771-3e8ed17fdc14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 20,
                "offset": 2,
                "shift": 5,
                "w": 3,
                "x": 217,
                "y": 68
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a86fae9e-d2b4-475c-9703-0bbf186a4294",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 222,
                "y": 68
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "2af8c4e7-cb84-4be2-b3fb-fab8a1bb719d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 79,
                "y": 46
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "1b3677fd-c04b-439a-aa81-904afcb8ef37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 175,
                "y": 24
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "1d07c51e-0fc3-40e4-ac43-238d3bd071b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 200,
                "y": 46
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f56eee2c-9d3c-4ee7-a496-5fe63a3de0ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 211,
                "y": 46
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "dbfe2625-ce27-48bf-96b6-58c1435941c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 20,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9f081ac1-47d0-4d9e-a1a3-eb2af55731b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 13,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "fda552e3-1c75-49eb-9209-1ddaed28ff01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 127,
                "y": 24
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "5c51ef78-979d-4992-bd2a-3fe6766a830a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "acbd1962-8573-4124-b37c-536aff878cc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b251fe3c-9989-491c-8cb0-565db442ab93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 91,
                "y": 24
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "633ace2c-6073-446d-953e-fa28e9934ee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 123,
                "y": 46
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1e89561e-c9e5-4195-b0c6-8df5f204b322",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e6a25fae-236d-4bd8-8d8f-30298e747eed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 79,
                "y": 24
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "42fec703-ef94-4988-bf48-f9197218c33f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 20,
                "offset": 2,
                "shift": 5,
                "w": 3,
                "x": 212,
                "y": 68
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ea3a8b9a-04af-45a0-b96f-1ea5bc8edc05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 42,
                "y": 68
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "426609fb-2cbb-4946-b9c7-707a95daddb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "bf76da23-77b6-4854-8a67-ff63824db985",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 68
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a9e02c3e-82ee-4986-80f1-86a5ba9792ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 20,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "506eac93-2288-4bc6-9442-7b346806152b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 139,
                "y": 24
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "8ffc9436-3335-4c83-9a85-b1b7025833e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "4f907d35-b8d5-4443-91a3-362c13e2877a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 67,
                "y": 24
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "65a9b9f5-58f5-465e-a3bb-b5ec51dcd334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "29284b04-74df-41f3-8c5e-3feb7a259a6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "98582041-7b85-4d72-bfa0-09220aa999a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 15,
                "y": 24
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e352c204-09c5-4a05-affb-c5938639dd6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 41,
                "y": 24
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b3dab8e5-c9d5-4cde-96d4-c30f95ebb7e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 103,
                "y": 24
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b4885065-ca21-4221-bc86-3062a3b2c701",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "19230090-2274-45e6-8b91-00c5a902571a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 20,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "42fd9f8d-cf04-4934-9e43-70a4e28c1e35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "3fa32cf6-6498-4a7d-9801-70543d09a006",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "614bf045-69ca-4fd1-82ed-ba6507f3861f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 115,
                "y": 24
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "31369a60-a13f-4e9c-ba57-af8f3e18d57e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 190,
                "y": 68
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "5153fc21-b574-4fc7-8434-fa0cf6e20d3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 164,
                "y": 68
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ac6bccba-911c-48ed-83b7-8ac9eb26d49f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 184,
                "y": 68
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a083004e-81fe-422b-95fd-daf9de109804",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 242,
                "y": 46
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "dbce7ca5-0976-44f2-bbf3-c3ccee4978af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 11,
                "x": 28,
                "y": 24
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a08cddfc-80b4-47f7-b9fc-58e7aacefb30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 196,
                "y": 68
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c9cf9b66-95cf-47f7-a156-3f4223460c4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 208,
                "y": 24
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "49136156-8772-4e86-ae37-127f1be681c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 222,
                "y": 46
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0733b4ee-a069-443a-a7dc-faf98e3c26c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 230,
                "y": 24
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "6e2efc54-15c6-4008-9a0a-cd927667d928",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 46
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "b2177bf2-d0ff-4860-91d3-3f125f84ca79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 46
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "21870d67-6d36-4b92-b0d3-bf66e652472d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 113,
                "y": 68
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b926908e-b761-4134-9192-cf56b3edd6ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 46
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "5c7b592e-7335-495b-833b-9b2b8c58de7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "60f35a52-4676-4697-8de1-a9e7a7e1cca9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 241,
                "y": 68
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "0b6f0d4a-d9b0-42a1-8317-d8c00e86393d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 20,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 178,
                "y": 68
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "95783998-c081-4e27-89eb-126b61d8a1fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 62,
                "y": 68
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b390ac5b-323e-484d-8f4e-5b36a86fbcea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 237,
                "y": 68
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "3a067e4d-420a-4411-bd5a-7e5a21490713",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 20,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "01c27567-dc63-41ae-a803-d8d6c7990a7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 232,
                "y": 46
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d4dd7426-5f63-41f4-bbdf-eb400404a97d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 197,
                "y": 24
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "49b44539-85b4-4e6d-b7f2-395862bdc260",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 52,
                "y": 68
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d6137a94-b85f-450b-a5a2-e150fb37116d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 46
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "3c70aab2-196d-45fd-a456-ec18ff20be2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 129,
                "y": 68
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "dad12022-ab0d-46eb-b253-31731b38d49d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 22,
                "y": 68
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "06a02f8b-2ca8-41de-b7cd-bfce5cd9b899",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 136,
                "y": 68
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "71839ec9-6bc7-47a2-92f5-aa47cc6dde4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 12,
                "y": 68
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ef153b8b-63f7-4008-9c40-a522ef589930",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 241,
                "y": 24
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ab6ac9d0-290d-4bde-85b1-0a7e649ce54c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1134c9d6-c51f-489d-9437-ccc8c4dbbb79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "72dd5807-2994-4dbf-9bbd-5106663447f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 46
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "efdd586d-40ba-4752-a52e-143a94e0c621",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 189,
                "y": 46
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "98e10f85-4592-48c4-bfae-89d7aeef6399",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 121,
                "y": 68
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7d18f61c-806e-4e9a-9aa8-c5c2c3285ca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 20,
                "offset": 2,
                "shift": 4,
                "w": 2,
                "x": 245,
                "y": 68
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1db93d23-a02d-455d-8c77-66bd77e1a65d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 97,
                "y": 68
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d00ee322-a5c2-47aa-b4a3-0ac9f72fb24e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 151,
                "y": 24
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "c6cc6ac3-9703-4795-9ed4-fe23cad3359f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "9427e825-a50b-405b-8e3e-0e80734dd1eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "d196e105-398a-4870-af4a-2ab32ccc6021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "48177994-bc83-4da2-9335-4494342093f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "75d38d27-38a5-41d1-99ef-4311319a3e7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "11285278-1630-45d7-ad59-d24f2e56990b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "959b666a-a35e-41fb-aa91-e54b0da83469",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "2d4c89ed-78b9-47b2-be5f-b46ced5e454b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "9e39349e-fb60-4451-9432-b381deb26a08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "4a420df9-b593-4f4a-893f-98c89d865677",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "d1950e98-9d9a-4225-b2b6-90f6f9471717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "c9aca8f2-b42c-4bd6-b1c1-50d62833518b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "462b595d-69a1-4d18-8972-42b7614c9e36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "ee3cca1d-017a-4a3e-b529-b59c2de3e470",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "07f03b94-3648-40ed-ac70-1a91fbabf00d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "ce6fe5b0-ab71-4ac1-84a6-1fdb082f499e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "a844377c-ed7f-4e2e-8592-5542feede1d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "f8dafcb1-2067-45b3-8ccf-76b8e99a7868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "9a16f747-cecf-49ea-83cf-1297e1f04330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "27f42202-848a-44ab-a417-e2ba211da85d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "f4e766df-62e7-4c6a-959a-86f20c1e83fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "9b7f256d-fc1d-4f8f-aa63-9bc4659cd1ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "c23f7ccc-0d56-4503-ae6c-3343189bfe4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "160a0014-e10f-41c3-885f-d1754819da39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "2edde85b-efdb-45b7-8e47-74d364ed3558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "6b7693c5-b422-4349-9f42-4615d4e9e459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "e7161358-1571-47c7-af36-2ae8feda3b48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "145ea246-82ba-451b-b222-7ea19c6ef23b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "4bac6e95-d112-4e71-9add-109d188a6aee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "2cf06098-cff5-4fe5-a1cb-77a017da2876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "91fca1ec-c779-48ae-93e9-1c2de98239fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "04b8d2a4-1438-46f8-964e-4b16e155b647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "49234a07-eed9-41d1-b051-6d108f3b8ae6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "67ec788c-8545-4771-947d-17e288babe9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "1aceeed0-4bd6-481f-9bf1-4b1dd5e9784c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "af1e43a4-e8ca-4627-a7bf-1a287c59e8e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "1004e2a2-1caa-44b1-80cb-311b8173cff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "c7d293f3-8c84-4b06-890a-fd1170853500",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "ee1429c1-96e1-4b01-82d3-318755786090",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "0af4bf04-7c84-40e4-b6fd-5f817d1ae37b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "0af3e3fb-72e4-48ca-b27d-e86ec551c3c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "e1c8e68c-e8bd-4cd4-b35e-a4fad6cb7de1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "7233326c-2f55-4218-a880-093002d71182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "1a89617a-a57b-48c2-83ce-626cd169ca2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "370d0c02-ad92-4836-83cc-5620864d655d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "0f75b170-4da7-4892-a4e9-a276bae87f0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "3f0c6056-1fbd-4cf0-8c85-f01a21ceac38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "bcc1a2ce-9988-47ce-bdda-b6899cdd6541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "66212453-49e0-480d-8bc3-a1e8b3ac9289",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "3bdbc26b-a27e-40bc-96c5-eca8853d2217",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "654bf582-86f9-493a-8918-9e95658c56df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "e5a07155-4d43-46fe-afac-ad14aeeb3230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "0e3f60a2-3221-423e-9b52-38fa3a84bf6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "6335924b-648e-4bb5-93ae-04fb1322baed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "0212f950-e7b2-4cd6-b1b8-30e4abafe0ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "ff7fbfc7-02b2-425f-b7f2-5f280363c5ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "c0c1b04a-0e0d-475a-8037-7dd7b43d615e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "ec686c98-ee73-44a3-8631-14c639a55d60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "b017876b-3441-48da-82c3-fffcb001044d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "0a15227b-077d-436d-b071-1426be06920e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "5745dd59-1f57-4641-95a5-98f913e0b8cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "4efeb810-ecb3-4ce5-96a5-2984c262d418",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "e15b96cb-7af2-452a-80d7-feaabb057b95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "714d2b5e-fb50-4025-b9d2-d10919d3b2ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "30faa9fc-36fc-4271-b46c-d84da5e102b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "dd74eecc-0186-4340-8619-5295850d48f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "36f81107-3a6c-4821-a8d1-7456374224ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "6ba88cdf-356b-4af6-b5b9-fecea6e32fbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "2dc06099-d3b8-42e7-8f2e-7c5a1d2093b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "e40e18cb-6d98-40f1-ab64-67476328880c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "3fcc9de2-fca1-48ae-9b65-4a98f8db8812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 13,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}