
scene_text = [
	
	"*BOOM* *CRASH*",
	"You hit a commet",
	"XD"

]

scene_info = [
	
	[scr_change_background,"R_BG",spr_cutscene_balk],
	[scr_cutscene_wait,1],
	[scr_create_object,obj_ufo_broken,450,40],
	[scr_move_object,obj_ufo_broken,450,40,230,420,4],
	[scr_cutscene_wait,7],
	[scr_change_background,"R_BG",spr_null],
	[scr_slide_transition,TRANS_MODE.GOTO, rm_planet_01]

]