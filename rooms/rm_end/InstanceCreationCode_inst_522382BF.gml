global.transition_done = false;

wait = 8;
text_speed = 0.3;

scene_text = [
	
	"Made by: Hidde en Wouter",
	"Informatica 27-03-2018"
]

scene_info = [
	
	[scr_play_sound,snd_end,1,false],
	[scr_cutscene_wait,30],
	[scr_slide_transition,TRANS_MODE.GOTO,rm_menu]

]