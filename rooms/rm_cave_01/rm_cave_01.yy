
{
    "name": "rm_cave_01",
    "id": "e9354df7-2762-4b1d-ae6d-5c8b73226742",
    "creationCodeFile": "RoomCreationCode.gml",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "bd90691c-fc10-4a3a-8513-34f2155d34b1",
        "c15e96cb-7a70-40ae-b2f7-993e64aca96d",
        "7d06ad73-3477-4d7e-98c4-531bdf2c8aaf",
        "b1ab1401-d41e-42ad-9421-7e9de4aaa900",
        "0debe790-550f-4ecc-855c-72f247cc1c31",
        "36b0cdc6-88dc-4e0f-b095-26c8412ca7b4",
        "49c3a65b-f68b-4496-9870-eac24b7de56d",
        "4579ef16-c316-4fac-a4ba-2ba30c0c090b",
        "ffa11718-2f22-4a07-adad-c299fac77c39",
        "3019218b-b792-4939-8b21-2563454c2893",
        "bb89e7dc-abbc-44cc-9640-b2b674c22e40",
        "e43ca136-8720-4964-9611-1b1b31087ce2",
        "0a2f62b7-db19-46b5-bd66-34d61476cc58",
        "88539be8-45c1-4485-8687-5a607727b664",
        "32bfb5f5-340e-4032-9d8c-aaf291fbc61a",
        "2c2c0db1-9c18-448d-a318-85b8f5c9d710"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Collide",
            "id": "6f728462-0c8c-4c67-bf12-3684ef1af4f1",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_5737116B","id": "bd90691c-fc10-4a3a-8513-34f2155d34b1","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5737116B","objId": "61b874e7-f379-470c-bdab-3724d5cdd891","properties": null,"rotation": 0,"scaleX": 0.765625,"scaleY": 1.265625,"mvc": "1.0","x": 124,"y": 23.45313},
{"name": "inst_69BA2F6","id": "c15e96cb-7a70-40ae-b2f7-993e64aca96d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_69BA2F6","objId": "61b874e7-f379-470c-bdab-3724d5cdd891","properties": null,"rotation": 0,"scaleX": 3.21875,"scaleY": 0.21875,"mvc": "1.0","x": 54,"y": 6},
{"name": "inst_2D6577EE","id": "7d06ad73-3477-4d7e-98c4-531bdf2c8aaf","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2D6577EE","objId": "61b874e7-f379-470c-bdab-3724d5cdd891","properties": null,"rotation": 0,"scaleX": 0.265625,"scaleY": 1.8125,"mvc": "1.0","x": 3.25,"y": 46.4375},
{"name": "inst_76D82273","id": "b1ab1401-d41e-42ad-9421-7e9de4aaa900","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_76D82273","objId": "61b874e7-f379-470c-bdab-3724d5cdd891","properties": null,"rotation": 0,"scaleX": 3.28125,"scaleY": 0.265625,"mvc": "1.0","x": 51.5,"y": 117.0469},
{"name": "inst_66460F65","id": "0debe790-550f-4ecc-855c-72f247cc1c31","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_66460F65","objId": "61b874e7-f379-470c-bdab-3724d5cdd891","properties": null,"rotation": 0,"scaleX": 0.265625,"scaleY": 1.5625,"mvc": "1.0","x": 196.25,"y": 41.6875},
{"name": "inst_59678040","id": "36b0cdc6-88dc-4e0f-b095-26c8412ca7b4","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_59678040.gml","creationCodeType": ".gml","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_59678040","objId": "dee41bbd-7a60-4446-8386-9fb40a6259fb","properties": null,"rotation": 0,"scaleX": 0.34375,"scaleY": 0.171875,"mvc": "1.0","x": 35.75,"y": 107.5938},
{"name": "inst_1C6A99AF","id": "49c3a65b-f68b-4496-9870-eac24b7de56d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1C6A99AF","objId": "61b874e7-f379-470c-bdab-3724d5cdd891","properties": null,"rotation": 0,"scaleX": 0.109375,"scaleY": 0.109375,"mvc": "1.0","x": 175.25,"y": 33.4375},
{"name": "inst_2CA8A9A1","id": "ffa11718-2f22-4a07-adad-c299fac77c39","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2CA8A9A1","objId": "61b874e7-f379-470c-bdab-3724d5cdd891","properties": null,"rotation": 0,"scaleX": 1.015625,"scaleY": 0.21875,"mvc": "1.0","x": 30.5,"y": 70.21875},
{"name": "inst_1AF4CC1F","id": "3019218b-b792-4939-8b21-2563454c2893","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1AF4CC1F","objId": "61b874e7-f379-470c-bdab-3724d5cdd891","properties": null,"rotation": 0,"scaleX": 0.5,"scaleY": 0.5,"mvc": "1.0","x": 40.25,"y": 25.45313}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Dialog",
            "id": "809632bf-0eff-4065-81c9-dce82d02970f",
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [

            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "6f653650-919c-49df-93d9-3c4b2d06c6fd",
            "depth": 200,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_587C378D","id": "4579ef16-c316-4fac-a4ba-2ba30c0c090b","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_587C378D","objId": "fd87f7c9-7b20-4816-9363-ae8ae0dda35b","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 176,"y": 34},
{"name": "inst_48C580A2","id": "bb89e7dc-abbc-44cc-9640-b2b674c22e40","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_48C580A2","objId": "025f70a5-d237-44ca-bba7-17b92aa44863","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 32,"y": 85},
{"name": "inst_3B662337","id": "e43ca136-8720-4964-9611-1b1b31087ce2","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3B662337","objId": "7d49ed3a-e01d-4cd3-9d84-018461fc94c5","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 192,"y": 0},
{"name": "inst_52A9FEF","id": "0a2f62b7-db19-46b5-bd66-34d61476cc58","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_52A9FEF","objId": "e6b0fca3-a08a-4849-bcbc-fb2e8d652d4a","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 64,"y": 32},
{"name": "inst_8D76922","id": "88539be8-45c1-4485-8687-5a607727b664","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_8D76922","objId": "e6b0fca3-a08a-4849-bcbc-fb2e8d652d4a","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 64,"y": 16},
{"name": "inst_5A434CFA","id": "32bfb5f5-340e-4032-9d8c-aaf291fbc61a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5A434CFA","objId": "e6b0fca3-a08a-4849-bcbc-fb2e8d652d4a","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 16,"y": 32},
{"name": "inst_E2CC200","id": "2c2c0db1-9c18-448d-a318-85b8f5c9d710","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_E2CC200","objId": "e6b0fca3-a08a-4849-bcbc-fb2e8d652d4a","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 16,"y": 16}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "name": "Cave",
            "id": "4caef1b3-4151-437a-8d53-b5c3ff8c1427",
            "depth": 300,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [
                {
                    "__type": "GMRTileLayer_Model:#YoYoStudio.MVCFormat",
                    "name": "rm_planet_03",
                    "id": "3d0673d9-e518-4119-b86d-13bc13a20b6f",
                    "depth": 400,
                    "grid_x": 32,
                    "grid_y": 32,
                    "hierarchyFrozen": false,
                    "hierarchyVisible": true,
                    "inheritLayerDepth": false,
                    "inheritLayerSettings": false,
                    "inheritSubLayers": false,
                    "inheritVisibility": false,
                    "layers": [

                    ],
                    "m_parentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "modelName": "GMRTileLayer",
                    "prev_tileheight": 0,
                    "prev_tilewidth": 0,
                    "mvc": "1.0",
                    "tiles": {
                        "SerialiseData": null,
                        "SerialiseHeight": 0,
                        "SerialiseWidth": 0,
                        "TileSerialiseData": [
                        ]
                    },
                    "tilesetId": "00000000-0000-0000-0000-000000000000",
                    "userdefined_depth": false,
                    "visible": true,
                    "x": 0,
                    "y": 0
                },
                {
                    "__type": "GMRTileLayer_Model:#YoYoStudio.MVCFormat",
                    "name": "CaveLayer01",
                    "id": "077036e9-e131-4c34-a7a1-047d9426efc0",
                    "depth": 500,
                    "grid_x": 32,
                    "grid_y": 32,
                    "hierarchyFrozen": false,
                    "hierarchyVisible": true,
                    "inheritLayerDepth": false,
                    "inheritLayerSettings": false,
                    "inheritSubLayers": false,
                    "inheritVisibility": false,
                    "layers": [

                    ],
                    "m_parentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "modelName": "GMRTileLayer",
                    "prev_tileheight": 16,
                    "prev_tilewidth": 16,
                    "mvc": "1.0",
                    "tiles": {
                        "SerialiseData": null,
                        "SerialiseHeight": 8,
                        "SerialiseWidth": 13,
                        "TileSerialiseData": [
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,0,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648
                        ]
                    },
                    "tilesetId": "fd1c4fe0-1f0a-4f5c-bef4-21f2010182d5",
                    "userdefined_depth": false,
                    "visible": true,
                    "x": 0,
                    "y": 0
                },
                {
                    "__type": "GMRTileLayer_Model:#YoYoStudio.MVCFormat",
                    "name": "rm_planet_02",
                    "id": "da56caca-71eb-4aa0-b6df-69053bfe7996",
                    "depth": 600,
                    "grid_x": 32,
                    "grid_y": 32,
                    "hierarchyFrozen": false,
                    "hierarchyVisible": true,
                    "inheritLayerDepth": false,
                    "inheritLayerSettings": false,
                    "inheritSubLayers": false,
                    "inheritVisibility": false,
                    "layers": [

                    ],
                    "m_parentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "modelName": "GMRTileLayer",
                    "prev_tileheight": 16,
                    "prev_tilewidth": 16,
                    "mvc": "1.0",
                    "tiles": {
                        "SerialiseData": null,
                        "SerialiseHeight": 8,
                        "SerialiseWidth": 13,
                        "TileSerialiseData": [
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,
                            2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648
                        ]
                    },
                    "tilesetId": "fd1c4fe0-1f0a-4f5c-bef4-21f2010182d5",
                    "userdefined_depth": false,
                    "visible": true,
                    "x": 0,
                    "y": 0
                }
            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRTileLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Tiles_2",
            "id": "0ce6df94-5fa3-4703-ad10-2f7761903880",
            "depth": 700,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRTileLayer",
            "prev_tileheight": 16,
            "prev_tilewidth": 16,
            "mvc": "1.0",
            "tiles": {
                "SerialiseData": null,
                "SerialiseHeight": 8,
                "SerialiseWidth": 13,
                "TileSerialiseData": [
                    72,60,60,60,60,60,60,73,40,72,60,60,73,
                    53,68,54,55,68,0,0,51,0,53,2147483648,2147483648,51,
                    53,68,62,63,68,0,0,51,0,53,2147483648,2147483648,51,
                    53,0,2147483648,0,0,2147483648,2147483648,51,2147483648,53,2147483648,2147483648,51,
                    53,74,74,74,74,2147483648,2147483648,59,60,61,2147483648,2147483648,51,
                    53,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,51,
                    53,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,2147483648,51,
                    2147483648,64,65,66,44,44,44,44,44,44,44,44,2147483648
                ]
            },
            "tilesetId": "fd1c4fe0-1f0a-4f5c-bef4-21f2010182d5",
            "userdefined_depth": false,
            "visible": true,
            "x": 0,
            "y": 0
        },
        {
            "__type": "GMRTileLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Tiles_1",
            "id": "62f25990-f701-4511-89a1-828ac4497ff7",
            "depth": 800,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRTileLayer",
            "prev_tileheight": 16,
            "prev_tilewidth": 16,
            "mvc": "1.0",
            "tiles": {
                "SerialiseData": null,
                "SerialiseHeight": 8,
                "SerialiseWidth": 13,
                "TileSerialiseData": [
                    52,52,52,52,52,52,52,52,52,52,52,52,52,
                    52,52,52,52,52,52,52,52,52,52,52,52,52,
                    52,52,52,52,52,52,52,52,52,52,52,52,52,
                    52,52,52,52,52,52,52,52,52,52,52,52,52,
                    52,52,52,52,52,52,52,52,52,52,52,52,52,
                    52,52,52,52,52,52,52,52,52,52,52,52,52,
                    52,52,52,52,52,52,52,52,52,52,52,52,52,
                    52,52,52,52,52,52,52,52,52,52,52,52,52
                ]
            },
            "tilesetId": "fd1c4fe0-1f0a-4f5c-bef4-21f2010182d5",
            "userdefined_depth": false,
            "visible": true,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "32de5958-dfe7-4f5a-833e-406453be9997",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": true,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 0,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "9d253c44-65ed-48c3-adf4-65fa2d75e5c6",
        "Height": 117,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": true,
        "mvc": "1.0",
        "Width": 208
    },
    "mvc": "1.0",
    "views": [
{"id": "541dbefa-792f-477a-b6a4-dccc58d8de91","hborder": 32,"hport": 720,"hspeed": -1,"hview": 117,"inherit": false,"modelName": "GMRView","objId": "025f70a5-d237-44ca-bba7-17b92aa44863","mvc": "1.0","vborder": 32,"visible": true,"vspeed": -1,"wport": 1280,"wview": 208,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "80ef3526-6d22-45c5-9b5b-1fd94b72c128","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "46455796-2cf7-41f3-8002-1f191f79c79f","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "a4b18bca-f8d4-4c5c-ac80-2f0ca144de84","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "b01223d8-53a2-4b2c-a885-f1309a5837ab","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "07f795f5-8f5e-4dd1-870d-ced8141a2af7","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "6e1baab4-15c3-4f92-9ecd-81be266d0c3c","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "ebe9d952-385c-41bc-9576-8d4d8b31aebd","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "3e7423b9-87c7-4a5b-a1d1-032417cb6e11",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}