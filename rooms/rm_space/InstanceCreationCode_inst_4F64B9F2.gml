scene_text = [
	
	"Wandering, alone in cold space",
	"You enter a stream of comets",
	"Will you survive??!"

]

scene_info = [
	
	[scr_change_background,"R_BG",spr_cutscene_temp],
	[scr_play_sound,snd_punch,4,false],
	[scr_cutscene_wait,0.3],
	[scr_create_object,obj_ufo,obj_ufo.x,obj_ufo.y],
	[scr_cutscene_wait,11],
	[scr_change_background,"R_BG",spr_null]

]