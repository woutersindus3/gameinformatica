

spawn_data = [
	
	[scr_spawn_met, -70, 160, 2, 1],
	[scr_spawn_wait, 2],
	[scr_spawn_met, -70, 260, 2.5, 2],
	[scr_spawn_wait, 1],
	[scr_spawn_met, -70, 160, 1.5, 2],
	[scr_spawn_wait, 2],
	[scr_spawn_met, -70, 80, 3, 1],
	[scr_spawn_wait, 0.5],
	[scr_spawn_met, -70, 200, 4, 1],
	[scr_spawn_wait, 1],
	[scr_spawn_met, -70, 60, 2, 2],
	[scr_spawn_wait, 1.5],
	[scr_spawn_met, -70, 100, 2, 1],
	[scr_spawn_wait, 0.5],
	[scr_spawn_met, -70, 320, 4, 1],
	[scr_spawn_met, -70, 160, 1.5, 2],
	[scr_spawn_wait, 0.5],
	[scr_spawn_met, -70, 280, 2, 2],
	[scr_spawn_wait, 0.5],
	[scr_spawn_met, -70, 80, 2.5, 1],
	[scr_spawn_wait, 4],
	[scr_spawn_met_giant, 1]



]